<?php
//ThinkPHP 配置
//define('SCAN_PATH',dirname(dirname(__FILE__)));
//其他配置（入口文件在根目录）
define('SCAN_PATH',dirname(__FILE__));
$config=file_get_contents('http://crm.nyjianzhan.com/api/checksite/get_config');
$scan=json_decode($config,true);
if(empty($scan)){
    echo json_encode([
        'code'=>0,
        'msg'=>'get config error',
        'data'=> '',
    ]);
    exit();
}
$scan_type=$scan['scan_type'];
$scan_func=$scan['scan_func'];
$scan_code=$scan['scan_code'];

$list = public_file_count(SCAN_PATH,$scan_type);
/*
if(file_exists(SCAN_PATH.'/scan_md5.log')){
    $old_md5=json_decode(file_get_contents(SCAN_PATH.'/scan_md5.log'),true);
    if(!empty($old_md5)) {
        $list = public_file_filter($list, $old_md5);
    }
}else{
    file_put_contents(SCAN_PATH.'/scan_md5.log',json_encode($list));
}
*/
$badfiles1=public_file_func($list,$scan_func);
$badfiles2=public_file_code($list,$scan_code);

$bad=array_merge($badfiles1,$badfiles2);
echo json_encode([
   'code'=>1,
   'msg'=>'ok',
   'data'=> $bad,
]);
//对要进行扫描的文件进行统计
/**
 * 对要进行扫描的文件进行统计
 *
 */
	 function public_file_count($dir,$file_type)
     {
         set_time_limit(120);
         $scan['file_type'] = explode('|', $file_type);
         $list = array();

         if (is_dir($dir)) {
             foreach ($scan['file_type'] as $k) {
                 $list = array_merge($list, scan_file_lists($dir . DIRECTORY_SEPARATOR, 1, $k, 0, 1, 1));
             }
         } else {
             $list = array_merge($list, array(str_replace(SCAN_PATH, '', $dir) => md5_file($dir)));
         }

         return $list;
     }
	//对文件进行筛选
	 function public_file_filter($scan_list,$old_md5)
     {
         foreach ($scan_list as $k => $v) {
             if ($v == $old_md5[$k]) {
                 unset($scan_list[$k]);
             }
         }
        return $scan_list;
     }
	
	//进行特征函数过滤
	 function public_file_func($file_list,$func) {
		@set_time_limit(600);
		if (isset($func) && !empty($func)) {
			foreach ($file_list as $key=>$val) {
				$html = file_get_contents(SCAN_PATH.$key);
				if(stristr($key,'.php.') != false || preg_match_all('/[^a-z]?('.$func.')\s*\(/i', $html, $state, PREG_SET_ORDER)) {
				    if(strpos($key,'vendor/phpoffice/phpexcel/Classes/PHPExcel'))continue;
                    if(strpos($key,'vendor/phpmailer/phpmailer'))continue;
					$badfiles[$key]['func'] = $state;
	            }
			}
		}
		if(!isset($badfiles)) $badfiles = array();
		return $badfiles;

	}
	
	//进行特征代码过滤
	 function public_file_code($file_list,$code) {
		@set_time_limit(600);

		$badfiles = [];
		if (isset($code) && !empty($code)) {
			foreach ($file_list as $key=>$val) {
				$html = file_get_contents(SCAN_PATH.$key);
				if(stristr($key, '.php.') != false || preg_match_all('/[^a-z]?('.$code.')/i', $html, $state, PREG_SET_ORDER)) {
					$badfiles[$key]['code'] = $state;
	            }
	            if(strtolower(substr($key, -4)) == '.php' && function_exists('zend_loader_file_encoded') && zend_loader_file_encoded(SCAN_PATH.$key)) {
	            	$badfiles[$key]['zend'] = 'zend encoded';
	            }
			}
		}
		return $badfiles;
	}
	
	 function scan_report() {
		$badfiles = getcache('scan_bad_file', 'scan');
		if (empty($badfiles)) {
			showmessage(L('scan_to_find_a_result_please_to_scan'), '?m=scan&c=index&a=init');
		}
		include $this->admin_tpl('scan_report');
	}
	
	 function view() {
		$url = isset($_GET['url']) && trim($_GET['url']) ? new_stripslashes(urldecode(trim($_GET['url']))) : showmessage(L('illegal_action'), HTTP_REFERER);
		$url = str_replace("..","",$url);
		
		if (!file_exists(SCAN_PATH.$url)) {
			showmessage(L('file_not_exists'));
		}
		$html = file_get_contents(SCAN_PATH.$url);
		//判断文件名，如果是database.php 对里面的关键字符进行替换
		$basename = basename($url);
		if($basename == "database.php" || $basename == "system.php"){
			//$html = str_replace();
			showmessage(L('重要文件，不允许在线查看！'));
		}
		$file_list = getcache('scan_bad_file', 'scan');
		if (isset($file_list[$url]['func']) && is_array($file_list[$url]['func']) && !empty($file_list[$url]['func'])) foreach ($file_list[$url]['func'] as $key=>$val)
		{
			$func[$key] = strtolower($val[1]);
		}
		if (isset($file_list[$url]['code']) && is_array($file_list[$url]['code']) && !empty($file_list[$url]['code'])) foreach ($file_list[$url]['code'] as $key=>$val)
		{
			$code[$key] = strtolower($val[1]);
		}
		if (isset($func)) $func = array_unique($func);
		if (isset($code)) $code = array_unique($code);
		$show_header = true;
 		include $this->admin_tpl('public_view');
	}
	
	 function md5_creat()
     {
         set_time_limit(120);
         $pro = isset($_GET['pro']) && intval($_GET['pro']) ? intval($_GET['pro']) : 1;
         pc_base::load_app_func('global');
         switch ($pro) {
             case '1'://统计文件
                 $msg = L('please_wait');
                 ob_start();
                 include $this->admin_tpl('md5_creat');
                 ob_flush();
                 ob_clean();
                 $list = scan_file_lists(SCAN_PATH, 1, 'php', 0, 1);
                 setcache('md5_' . date('Y-m-d'), $list, 'scan');
                 echo '<script type="text/javascript">location.href="?m=scan&c=index&a=md5_creat&pro=2&pc_hash=' . $_SESSION['pc_hash'] . '"</script>';
                 break;

             case '2':
                 showmessage(L('viewreporttrue'), '?m=scan&c=index&a=init');
                 break;
         }
     }
/**
 * 文件扫描
 * @param $filepath     目录
 * @param $subdir       是否搜索子目录
 * @param $ex           搜索扩展
 * @param $isdir        是否只搜索目录
 * @param $md5			是否生成MD5验证码
 * @param $enforcement  强制更新缓存
 */
function scan_file_lists($filepath, $subdir = 1, $ex = '', $isdir = 0, $md5 = 0, $enforcement = 0)
{
    static $file_list = array();
    if ($enforcement) {
        $file_list = array();
    }
    $flags = $isdir ? GLOB_ONLYDIR : 0;
    $list = glob($filepath . '*' . (!empty($ex) && empty($subdir) ? '.' . $ex : ''), $flags);
    if (!empty($ex)) {
        $ex_num = strlen($ex);
    }
    foreach ($list as $k => $v) {
        $v1 = str_replace(SCAN_PATH, '', $v);
        if ($subdir && is_dir($v)) {
            scan_file_lists($v . DIRECTORY_SEPARATOR, $subdir, $ex, $isdir, $md5);
            continue;
        }
        if (!empty($ex) && strtolower(substr($v, -$ex_num, $ex_num)) == $ex) {
            if ($md5) {
                $file_list[$v1] = md5_file($v);
            } else {
                $file_list[] = $v1;
            }
            continue;
        } elseif (!empty($ex) && strtolower(substr($v, -$ex_num, $ex_num)) != $ex) {
            unset($list[$k]);
            continue;
        }
    }
    return $file_list;
}