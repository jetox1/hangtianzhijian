<?php
/**
 * Wchat.php
 * Niushop商城系统
 */

namespace app\wchat\controller;
use think\Controller;
use clt\WchatOauth;
class Index extends Controller
{
    function order(){
        return view();
    }
    function category($id=null){
        $categroy = db('category')->where('parentid',$id)->select();
        $this->assign('lists',$categroy);
        return view();
    }
    function zhengshu($catid=null){
        $this->assign('id',$catid);
        return view();
    }
    function ajax_zhengshu($id=null){
        $info = db('page')->where('id',$id)->find();
        $data['code'] = 0;
        $data['data'] = $info;
        return json($data);
    }
    function ywfw($catid=null){
            $list = db('zizhinengli')->where('catid',$catid)->select();
            $this->assign('lists',$list);
            return view();
    }
    function jcsb($catid=null){
        $list = db('zizhinengli')->where('catid',$catid)->select();
        $this->assign('lists',$list);
        return view();
    }
    function ywfb($catid=null){
        $this->assign('id',$catid);
        return view();
    }
    function ajax_ywfb($id=null){
        $info = db('page')->where('id',$id)->find();
        $data['code'] = 0;
        $data['data'] = $info;
        return json($data);
    }
    function jcyj($catid=null){
        $this->assign('id',$catid);
        return view();
    }
    function ajax_jcyj($id=null){
        $info = db('page')->where('id',$id)->find();
        $data['code'] = 0;
        $data['data'] = $info;
        return json($data);
    }
    function jianjie($id=null){
        $this->assign('id',$id);
        return view();
    }
    function ajax_jianjie($id=null){
        $info = db('page')->where('id',$id)->find();
        $data['code'] = 0;
        $data['data'] = $info;
        return json($data);
    }
    function jcxm($pid=0){
        $pid = intval($pid);
        $jccategory = db('jc_category')->where('pid',$pid)->select();
        $this->assign('lists',$jccategory);
        if ($pid!=0){
            $this->assign('detail',1);
        }
        return view();
    }
    function jcxm_detail($catid=null){
        $jcxiangmu = db('jcxiangmu')->where('catid',$catid)->Select();
        $this->assign('lists',$jcxiangmu);
        return view();
    }
    function show_jcxm_info($catid=null){
        $info =db('jcxiangmu')->where('id',$catid)->find();
        $info['catename'] = $this->get_catname($info['catid']);
        $info['parent'] = $this->get_parent($info['catid']).'-'.$info['title'];
        var_dump($info);
        die;
        $this->assign('info',$info);
        return view();
    }
    function fbxq(){
        if(request()->post()){
            $data = input('post.');
            $order = [
                'title' => $data['title'],
                'uid'   =>session('user.id'),
                'endtime' => time($data['endtime']),
                'time'=>time(),
                'company' => $data['company'],
                'address' => $data['address'],
                'r_name' => $data['r_name'],
                'r_address' => $data['r_address'],
                'r_phone' => $data['r_phone'],
                'code'   =>$data['code'],
                'chuli_type' => $data['chuli_type'],
                'receipt' => $data['receipt'],
                'intelligence' =>json_encode($data['intelligence']),
                'subpackage' => $data['subpackage'],
                'format' => $data['format'],
                'way' => $data['way'],
                'urgent' => $data['urgent'],
                'file' => $data['file'],
                'order_Sn'=>time()
            ];
            $xiangmu = [
                'province' => $data['data']['province'],
                'city' => $data['data']['city'],
                'area' => $data['data']['area'],
                'caizhi' => $data['data']['caizhi'],
                'xinghao' => $data['data']['xinghao'],
                'other' => $data['data']['other'],
                'num' => $data['data']['num'],
                'content' => $data['data']['content'],
            ];
            db('orders')->insert($order);
            $order_id = db('orders')->getLastInsID();
            if ($order_id){
                $xiangmu['order_id'] = $order_id;
                $good_id = db('order_goods')->insert($xiangmu);
                if ($good_id){
                    $result = [
                        'code' => '1',
                        'msg'  => '发布成功',
                        'order_id'=>$order['order_Sn']
                    ];
                    return json($result);
                }
            }
        }else{
            if (!session('user.id')) {
                $this->redirect('index/login');
            }else{
                $p =  db('jc_category')->where('pid', 0)->select();
                $this->assign('p', $p);
                return view();
            }
        }
    }
    function login(){
        if (request()->isPost()){
            $table = db('users');
            $username = input('username');
            $password = input('password');
            $user = $table->where("mobile",$username)->whereOr('email',$username)->find();
            if(!$user){
                return array('code'=>0,'msg'=>'账号不存在!');
            }elseif(md5($password) != $user['password']){
                return array('code'=>0,'msg'=>'密码错误!');
            }elseif($user['is_lock'] == 1){
                return array('code'=>0,'msg'=>'账号异常已被锁定！！！');
            }else{
                $sessionUser = $table->where("id",$user['id'])->field('id,username')->find();
                session('user',$sessionUser);
                return array('code'=>1,'msg'=>'登录成功','url' => url('index/fbxq'));
            }
        }else{
            return view();
        }
    }
    function forget(){
        return view();
    }
    function verify_Code($code=null){
        if ($code == session('smscode')){
            session('code',null);
            return json(['code'=>0,'msg'=>'验证码正确','url'=>url('forget_step2')]);
        }
    }
    function forget_step2($password=null){
        if (request()->isPost()){
            $mobile  = session('mobile');
            $data['mobile'] = $mobile;
            $data['password']=md5($password);
            if (db('users')->where('mobile',$mobile)->update($data)){
                return json(['code'=>0,'msg'=>'修改密码成功','url'=>url('login')]);
            }else{
                return json(['code'=>0,'msg'=>'修改密码失败','url'=>url('login')]);
            }
        }else{
            return view();
        }
    }
    function reg(){
        if (request()->isPost()){
            $data = input('post.');
            $is_validated = 0 ;
            if(is_email($data['mobile'])){
                $is_validated = 1;
                $map['email_validated'] = 1;
                $map['email'] = $data['mobile']; //邮箱注册
            }
            if(is_mobile_phone($data['mobile'])){
                $is_validated = 1;
                $map['mobile_validated'] = 1;
                $map['mobile'] = $data['mobile']; //手机注册
            }
            if($is_validated != 1){
                return array('code'=>0,'msg'=>'请用手机号或邮箱注册');
            }
            if(!$data['username'] || !$data['password']){
                return array('code'=>-1,'msg'=>'请输入昵称或密码');
            }
            //验证是否存在用户名
            if(get_user_info($data['email'],1)||get_user_info($data['mobile'],2)){
                return array('code'=>-1,'msg'=>'账号已存在');
            }
            $map['username'] = $data['username'];
            $map['password'] = md5($data['password']);
            $map['reg_time'] = time();
            $map['type']  = $data['type'];
            $map['token'] = md5(time().mt_rand(1,99999));
            $id = db('users')->insertGetId($map);
            if($id === false){
                return array('code'=>-1,'msg'=>'注册失败');
            }
            $user = db('users')->field('id,username,type')->where("id", $id)->find();
            session('user',$user);
            session('user',$user);
            return array('code'=>1,'msg'=>'注册成功','result'=>$user);
        }else{
            return view();
        }
    }
    function search($order_id=null,$type=null){
        if (request()->post()){
            $order_sn = htmlspecialchars($order_id);
            $info = db('orders')->where('order_Sn',$order_sn)->find();
            $offlineinfo = db('offline_orders')->where('tatchsn|reportsn',$order_sn)->find();
            if ($info == null){
                if($offlineinfo == null){
                    $data['code'] = 0;
                    $data['msg'] = $order_sn;
                }else{
                    $jindu = get_offline_jindu($offlineinfo['id']);
                    $data['code'] = 1;
                    $data['data'] = $jindu;
                }
            }else{
                $jindu = get_jindu($info['id']);
                $data['code'] = 1;
                $data['data'] = $jindu;
            }
            return json($data);
        }else{
            if ($type == 1){
                $this->assign('title','检测进度查询');
            }
            if ($type == 2){
                $this->assign('title','报告结果查询');
            }
            if ($type == 3){
                $this->assign('title','报告查询下载');
            }

            return view('search');
        }
    }
    function sendcode($mobile=null){
        $code = rand(100000,999999);
        $content = '【航天智检】验证码：'.$code.'，请在5秒分钟内进行操作。';
        $send = send_sms($mobile,$content);
        if ($send) {
            session('mobile',$mobile);
            session('smscode',$code);
            return json(['code' => 0, 'msg' => $send]);
        } else {
            return json(['code' => 1, 'msg' => $send]);
        }
    }
}