<?php
namespace app\user\controller;
use think\Input;
class Set extends Common{
    public function _initialize(){
        parent::_initialize();
        if (!session('user.id')) {
            $this->redirect('login/index');
        }
    }
    public function index(){
        if(request()->isPost()){
            $data = input('post.');
            $user = db('users');
            $oldEmail = $user->where(['id'=>$data['id']])->value('email');
            if($oldEmail != $data['eamil']){
                $data['email_validated'] = 0;
            }
            if (db('users')->update($data)!==false) {
                $result['msg'] = '编辑资料成功!';
                $result['status'] = 1;
            } else {
                $result['msg'] = '编辑资料失败!';
                $result['status'] = 0;
            }
            return $result;
        }else{
            $province = db('Region')->where ( array('pid'=>1) )->select ();
            $this->assign('province',$province);
            $city = db('Region')->where ( array('pid'=>$this->userInfo['province']) )->select ();
            $this->assign('city',$city);
            $district = db('Region')->where ( array('pid'=>$this->userInfo['city']) )->select ();
            $this->assign('district',$district);
            $this->assign('title','基本设置');
            $this->assign('page_name','set');
            return $this->fetch();
        }
    }
    public function getRegion(){
        $Region=db("region");
        $map['pid'] = input("pid");
        $list=$Region->where($map)->select();
        return $list;
    }
    public function avatar(){
        $data = input('post.');
        db('users')->where(['id'=>session('user.id')])->update($data);
        return true;
    }
    /**
     * 修改密码
     * @param $old_password  旧密码
     * @param $new_password  新密码
     * @param $confirm_password 确认新 密码
     */
    public function repass(){
        $old_password  = input('post.nowpass');
        $new_password = input('post.pass');
        $confirm_password = input('post.repass');
        if(empty($old_password) || empty($new_password) ||empty($confirm_password)){
            return array('status'=>0,'msg'=>'请输入完整信息');
        }
        if($new_password != $confirm_password)
            return array('status'=>0,'msg'=>'两次密码输入不一致');
        if(!preg_match('/(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,30}/',$new_password)) {
            return array('code'=>0,'msg'=>'密码为8-30个字母数字特殊字符组合');
        }
        //验证原密码
        $user = $this->userInfo;
        if(!empty($user['salt'])){
            if(md5(md5($old_password).$user['salt'].$user['salt'].$user['salt']) != $user['password']){
                return array('code'=>0,'msg'=>'当前密码错误!');
            }
        }else{
            if(md5($old_password) != $user['password']){
                return array('code'=>0,'msg'=>'当前密码错误!');
            }
        }
        $rand = rand(100000,999999);
        $md5str = md5($rand);
        $salt = substr($md5str,0,4);
        $map['salt'] = $salt;
        $map['password'] = md5(md5($new_password).$map['salt'].$map['salt'].$map['salt']);
        if(db('users')->where("id", session('user.id'))->update($map)!==false){
            return array('status'=>1,'msg'=>'修改成功','action'=>url('index'));
        }else{
            return array('status'=>0,'msg'=>'修改失败');
        }
    }
    public function unbind(){
        $data['oauth']='';
        $data['openid']='';
        $data['token']='';
        db('users')->where("id", session('user.id'))->update($data);
        return array('status'=>1,'msg'=>'QQ已解绑','action'=>url('index'));
    }
}