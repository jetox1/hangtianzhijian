<?php

namespace app\user\controller;

use think\Input;
use think\Request;
use think\Db;

class Sysrz extends Common
{
    public function _initialize(){
        parent::_initialize();
        if (!session('user.id')) {
            $this->redirect('login/index');
        }
    }
    public function index(){
        $result = db('sysrz')->where('uid',$_SESSION['think']['user']['id'])->find();
        if ($result){
            $this->redirect(url('User/sysrz/shebei'));
        }else{
            $p = db('area')->where('parentid', 1)->select();
            $this->assign('p', $p);
            return view();
        }

    }
    public function add(){
        $data = input('post.');
        $data['zzry'] = json_encode($data['zzry'],true);
        $data['yyzz'] = json_encode($data['yyzz'],true);
        $data['location'] = $this->get_location($data['address']);
        $data['status'] = '0';
        $data['uid'] = $_SESSION['think']['user']['id'];
        $id = db('sysrz')->insert($data);
        if ($id){
            return json(['code'=>'1','msg'=>'申请成功']);
        }else{
            return json(['code'=>'0','msg'=>'申请失败']);
        }
    }

    public function get_location($address){
        $return = file_get_contents('http://api.map.baidu.com/geocoder/v2/?address='.$address.'&output=json&ak=yXTsBdAa2G5TswAB42SNGzgZ');
        $result = json_decode($return,true);
        $location = $result['result']['location'];
        return $location;
    }
    public function guanli(){
        $result = db('jc_category')->select();
        $this->assign('category',$result);
        return view();
    }
    public function shebei(){
        $this->assign('page_name','sys');
        $this->assign('child_name','shebei');
        return view();
    }
    public function shebei_handle(){
        $data = input('post.');
        $data['uid']=$_SESSION['think']['user']['id'];
        if (db('shebei')->insert($data)){
            return json(['code'=>1,'msg'=>'添加成功']);
        }else{
            return json(['code'=>0,'msg'=>'添加失败']);
        }
    }
    public function ajax_shebei(){
        $result = db('shebei')->where('uid',$_SESSION['think']['user']['id'])->select();
        return json(['code'=>0,'count'=>count($result),'data'=>$result]);
    }
    public function jcxm(){
        $this->assign('page_name','sys');
        $this->assign('child_name','jcxm');
        return view();
    }
    public function add_xiangmu(){
        if(request()->isPost()){
            $uid = $_SESSION['think']['user']['id'];
            $sys = db('sysrz')->where('uid',$uid)->find();
            $data = input('post.');
            $arr = explode('-',$data['jid']);
            $map=[
                'jid'   => $arr[0],
                'desc'  =>$data['desc'],
                'jcyj'  =>$data['jcyj'],
                'sort'  => $data['sort'],
                'sid'   =>$sys['id'],
            ];
            $result = db('xiangmu')->insert($map);
            if($result){
                return json(['code'=>1,'msg'=>'添加成功']);
            }else{
                return json(['code'=>0,'msg'=>'添加失败']);
            }
        }else{
            $arr = db('jc_category')->order('id')->select();
            $lists = get_tree($arr);
            $this->assign('jccategory',$lists);
            return $this->fetch();
        }
    }
    public function ajax_xiangmu(){
        $uid = $_SESSION['think']['user']['id'];
        $sys = db('sysrz')->where('uid',$uid)->find();
        $join = [
            ['clt_jc_category w','x.jid = w.id'],
            ['clt_jc_category q','w.pid = q.id'],
        ];
        $result = Db::table('clt_xiangmu')->alias('x')
            ->join($join)
            ->where('sid',$sys['id'])
            ->field("x.*,w.name as jid,q.name as pid")
            ->select();
        return json(['code'=>0,'count'=>count($result),'data'=>$result]);
    }
    public function base(){
        if (Request()->isPost()){
            $data = input('post.');
            if ($data['id']==null||$data['id']==''){
                $data['uid']=$_SESSION['think']['user']['id'];
                $res = db('sysrz')->insert($data);
            }else{
                $res = db('sysrz')->update($data);
            }
            if ($res){
                return json(['code'=>1,'msg'=>'修改成功']);
            }else{
                return json(['code'=>0,'msg'=>'修改失败']);
            }
        }else{
            $uid = $_SESSION['think']['user']['id'];
            $sys = db('sysrz')->where('uid',$uid)->find();
            $category = db('jc_category')->where('pid',0)->select();
            $this->assign('category',$category);
            $this->assign('info',$sys);
            $this->assign('page_name','sys');
            $this->assign('child_name','base');
            return $this->fetch();
        }
    }
    public function ajax_region()
    {
        $id = input('id');
        $jc_category = db('area')->where('parentid',$id)->select();
        return json($jc_category);
    }
}