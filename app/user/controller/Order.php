<?php

namespace app\user\controller;

use think\Input;
use think\Request;
use think\Db;

class Order extends Common
{

    public function _initialize()
    {
        parent::_initialize();
        if (!session('user.id')) {
            $this->redirect('login/index');
        }
    }

    public function index($id=null)
    {
        $db = db('jc_category');
        $p = $db->where('pid', 0)->select();
        $this->assign('p', $p);
        return $this->fetch('add');
    }

    public function ajax_category()
    {
        $id = input('id');
        $jc_category = db('jc_category')->where('pid',$id)->select();
        return json($jc_category);
    }
    public function ajax_xiangmu()
    {
        $id = input('id');
        $jc_category = db('jcxiangmu')->where('catid',$id)->select();
        return json($jc_category);
    }
    public function ajax_order()
    {
        $page =input('page')?input('page'):1;
        $key = input('key/d');
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $sql = Db::table('clt_orders')->alias('o')->where(['uid'=>session('user.id')]);
        if($key){
            $sql = $sql->where('o.order_Sn','like',"%$key%")->whereOr('o.tatchsn','like',"%$key%")
                ->whereOr('o.reportsn','like',"%$key%");
        }
        $data = $sql->order('o.id desc')->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();

        foreach($data['data'] as $v){
            $v['chuli_type']=get_chuli_type($v['chuli_type']);
            $v['receipt']=get_receipt($v['receipt']);
            $v['subpackage']=get_subpackage($v['subpackage']);
            $v['format']=get_format($v['format']);
            $v['way']=get_way($v['way']);
            $v['urgent']=get_urgent($v['urgent']);
            $v['endtime']=date('Y-m-d',$v['endtime']);
            $v['status']=get_status($v['status']);
            $item[] = $v;
        }

        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' => $data['total'],
                'data' => $item,
            ];

        }else{
            $res = [
                'code' => '0',
                'msg' => '没有已完成订单',
            ];
        }
        return json($res);
    }
    public function ajax_order_pre()
    {
        $page =input('page')?input('page'):1;
        $key = input('key/d');
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $sql = Db::table('clt_orders')->alias('o')->where(['uid'=>session('user.id')]);
        if($key){
            $sql = $sql->where('o.order_Sn','like',"%$key%")->whereOr('o.tatchsn','like',"%$key%")
                ->whereOr('o.reportsn','like',"%$key%");
        }
        $sql = $sql->where('o.status',0);
        $data = $sql->order('o.id desc')->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();

        foreach($data['data'] as $v){
            $v['chuli_type']=get_chuli_type($v['chuli_type']);
            $v['receipt']=get_receipt($v['receipt']);
            $v['subpackage']=get_subpackage($v['subpackage']);
            $v['format']=get_format($v['format']);
            $v['way']=get_way($v['way']);
            $v['urgent']=get_urgent($v['urgent']);
            $v['endtime']=date('Y-m-d',$v['endtime']);
            $v['status']=get_status($v['status']);
            $item[] = $v;
        }

        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' => $data['total'],
                'data' => $item,
            ];

        }else{
            $res = [
                'code' => '0',
                'msg' => '没有已完成订单',
            ];
        }
        return json($res);
    }
    public function ajax_order_ing()
    {
        $page =input('page')?input('page'):1;
        $key = input('key/d');
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $sql = Db::table('clt_orders')->alias('o')->where(['uid'=>session('user.id')]);
        if($key){
            $sql = $sql->where('o.order_Sn','like',"%$key%")->whereOr('o.tatchsn','like',"%$key%")
                ->whereOr('o.reportsn','like',"%$key%");
        }
        $sql = $sql->where('o.status',1);
        $data = $sql->order('o.id desc')->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();

        foreach($data['data'] as $v){
            $v['chuli_type']=get_chuli_type($v['chuli_type']);
            $v['receipt']=get_receipt($v['receipt']);
            $v['subpackage']=get_subpackage($v['subpackage']);
            $v['format']=get_format($v['format']);
            $v['way']=get_way($v['way']);
            $v['urgent']=get_urgent($v['urgent']);
            $v['endtime']=date('Y-m-d',$v['endtime']);
            $v['status']=get_status($v['status']);
            $item[] = $v;
        }

        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' => $data['total'],
                'data' => $item,
            ];

        }else{
            $res = [
                'code' => '0',
                'msg' => '没有已完成订单',
            ];
        }
        return json($res);
    }
    public function ajax_order_over()
    {
        $page =input('page')?input('page'):1;
        $key = input('key/d');
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $sql = Db::table('clt_orders')->alias('o')->where(['uid'=>session('user.id')]);
        if($key){
            $sql = $sql->where('o.order_Sn','like',"%$key%")->whereOr('o.tatchsn','like',"%$key%")
                ->whereOr('o.reportsn','like',"%$key%");
        }
        $sql = $sql->where('status',2);
        $data = $sql->order('o.id desc')->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();

        foreach($data['data'] as $v){
            $v['chuli_type']=get_chuli_type($v['chuli_type']);
            $v['receipt']=get_receipt($v['receipt']);
            $v['subpackage']=get_subpackage($v['subpackage']);
            $v['format']=get_format($v['format']);
            $v['way']=get_way($v['way']);
            $v['urgent']=get_urgent($v['urgent']);
            $v['endtime']=date('Y-m-d',$v['endtime']);
            $v['status']=get_status($v['status']);
            $item[] = $v;
        }

        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' => $data['total'],
                'data' => $item,
            ];

        }else{
            $res = [
                'code' => '0',
                'msg' => '没有已完成订单',
            ];
        }
        return json($res);
    }
    public function addhandle()
    {

        if (request()->isPost()) {
            $data = input();
            $jc_cate = input('jc_cate/a');
            unset($data['jc_cate']);
            $order = [
                'title' => $data['title'],
                'uid'   =>$_SESSION['think']['user']['id'],
                'endtime' => strtotime($data['endtime']),
                'time'=>time(),
                'company' => $data['company'],
                'address' => $data['address'],
                'r_name' => $data['r_name'],
                'r_address' => $data['r_address'],
                'r_phone' => $data['r_phone'],
                'code'   =>$data['code'],
                'chuli_type' => $data['chuli_type'],
                'receipt' => $data['receipt'],
//                'intelligence' =>json_encode($data['intelligence']),
                'subpackage' => $data['subpackage'],
                'format' => $data['format']?$data['format']:'',
                'way' => $data['way'],
                'urgent' => $data['urgent'],
                'file' => $data['file'],
                'order_Sn'=> $this->makeOrderSn(),
                'tatchsn' => $data['tatchsn'],
                'jc_cate' => json_encode($jc_cate)
            ];
            $xiangmu = [
                'province' => $data['data']['province'],
                'city' => $data['data']['city'],
                'area' => $data['data']['area'],
                'caizhi' => $data['data']['caizhi'],
                'xinghao' => $data['data']['xinghao'],
                'other' => $data['data']['other'],
                'num' => $data['data']['num'],
                'content' => $data['data']['content'],
            ];
            db('orders')->insert($order);
            $order_id = db('orders')->getLastInsID();
            if ($order_id){
                $xiangmu['order_id'] = $order_id;
                $good_id = db('order_goods')->insert($xiangmu);
                if ($good_id){
                    $result = [
                        'code' => '1',
                        'msg'  => '发布成功',
                        'order_id'=>$order['order_Sn']
                    ];
                    return json($result);
                }
            }
        }
    }
    public function addjindu()
    {
        if (request()->isPost()) {
            $data['order_id'] = input('order_id');
            $data['desc'] = input('desc');
            $data['file'] = input('file');
            $data['time'] = date('Y-m-d h:i:s',time());
            $order_id = db('order_jindu')->insert($data);
            if ($order_id){
                    $result = [
                        'code' => '1',
                        'msg'  => '发布成功',
                    ];
            }else{
                $result = [
                    'code' => '1',
                    'msg'  => '发布成功',
                ];
            }
            return json($result);
        }
    }
    public function ajax_get_order()
    {
        $key = input('key');
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $sql = Db::table('clt_user_order')->alias('o')
            ->where('user_id', session('user.id'))
            ->join('clt_orders w','o.order_id=w.id');
        if($key){
            $sql = $sql->where('w.order_Sn','like',"%$key%")->whereOr('w.tatchsn','like',"%$key%")
                ->whereOr('w.reportsn','like',"%$key%");
        }
        $data = $sql->field('w.*')->order('id desc')->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();

        foreach($data['data'] as $v){
            $v['chuli_type']=get_chuli_type($v['chuli_type']);
            $v['receipt']=get_receipt($v['receipt']);
            $v['subpackage']=get_subpackage($v['subpackage']);
            $v['format']=get_format($v['format']);
            $v['way']=get_way($v['way']);
            $v['urgent']=get_urgent($v['urgent']);
            $v['status']=get_status($v['status']);
            $v['endtime']=date('Y-m-d',$v['endtime']);
         //   $v['endtime']=date('Y-m-d',$v['endtime']);
            $item[] = $v;
        }
        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' =>$data['total'],
                'data' => $item,
            ];
            return json($res);
        }
    }
    public function ajax_get_order_ing()
    {
        $key = input('key');
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $sql = Db::table('clt_user_order')->alias('o')
            ->where('user_id', session('user.id'))
            ->join('clt_orders w','o.order_id=w.id');
        if($key){
            $sql = $sql->where('w.order_Sn','like',"%$key%")->whereOr('w.tatchsn','like',"%$key%")
                ->whereOr('w.reportsn','like',"%$key%");
        }
        $data = $sql->where('w.status',1)->field('w.*')->order('id desc')->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();

        foreach($data['data'] as $v){
            $v['chuli_type']=get_chuli_type($v['chuli_type']);
            $v['receipt']=get_receipt($v['receipt']);
            $v['subpackage']=get_subpackage($v['subpackage']);
            $v['format']=get_format($v['format']);
            $v['way']=get_way($v['way']);
            $v['urgent']=get_urgent($v['urgent']);
            $v['status']=get_status($v['status']);
            $v['endtime']=date('Y-m-d',$v['endtime']);
            //   $v['endtime']=date('Y-m-d',$v['endtime']);
            $item[] = $v;
        }
        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' =>$data['total'],
                'data' => $item,
            ];
            return json($res);
        }
    }
    public function ajax_get_order_over()
    {
        $key = input('key');
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $sql = Db::table('clt_user_order')->alias('o')
            ->where('user_id', session('user.id'))
            ->join('clt_orders w','o.order_id=w.id');
        if($key){
            $sql = $sql->where('w.order_Sn','like',"%$key%")->whereOr('w.tatchsn','like',"%$key%")
                ->whereOr('w.reportsn','like',"%$key%");
        }
        $data = $sql->where('w.status',2)->field('w.*')->order('id desc')->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();

        foreach($data['data'] as $v){
            $v['chuli_type']=get_chuli_type($v['chuli_type']);
            $v['receipt']=get_receipt($v['receipt']);
            $v['subpackage']=get_subpackage($v['subpackage']);
            $v['format']=get_format($v['format']);
            $v['way']=get_way($v['way']);
            $v['urgent']=get_urgent($v['urgent']);
            $v['status']=get_status($v['status']);
            $v['endtime']=date('Y-m-d',$v['endtime']);
            //   $v['endtime']=date('Y-m-d',$v['endtime']);
            $item[] = $v;
        }
        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' =>$data['total'],
                'data' => $item,
            ];
            return json($res);
        }
    }
    function show(){
        $id = input('id');
        $data = db('orders')->where('id',$id)->find();
        $data['chuli_type']=get_chuli_type($data['chuli_type']);
        $data['receipt']=get_receipt($data['receipt']);
        $data['intelligence']=json_decode($data['intelligence']);
        $data['subpackage']=get_subpackage($data['subpackage']);
        $data['format']=get_format($data['format']);
        $data['way']=get_way($data['way']);
        $data['urgent']=get_urgent($data['urgent']);
        if ($data){
            return json(['code'=>1,'msg'=>'显示成功','data'=>$data]);
        }
    }
    function ajax_order_show(){
        $id = input('id');
        $res = Db::table('clt_order_goods')
            ->where('order_id',$id)
            ->select();
        foreach($res as $item){
            $order_sn = db('orders')->where('id',$item['order_id'])->column('order_Sn');
            $item['order_id'] = $order_sn[0];
            $item['province'] = get_Category_name($item['province']);
            $item['city'] = get_Category_name($item['city']);
            $item['area'] = get_xiangmu_name($item['area']);
            $data[] = $item;
        }


        if ($data){
            $data = [
                'code' => '0',
                'msg' => '获取成功',
                'count' => count($data),
                'data' => $data,
            ];

        }else{
            $data = [
                'code' => '0',
                'msg' => '获取失败',
                'count' => count($data),
                'data' => $data,
            ];
        }
        return json($data);
    }
    function del_order($id){
        $id = input('id');
        $res = DB::table('clt_orders')->where('id',$id)->delete();
        if ($res){
            $data = [
                'code'=> '1',
                'msg'=>'取消成功',
            ];
        }else{
            $data = [
                'code'=>'0',
                'msg'=>'取消失败请联系管理员',
            ];
        }
        return json($data);
    }

    public function makeOrderSn($sn = null)
    {
        /* 选择一个随机的方案 */
        mt_srand((double)microtime() * 1000000);
        if ($sn)
            return substr($sn, 0, 14) . str_pad(mt_rand(1, 99999), 6, '0', STR_PAD_LEFT);
        return date('YmdHis') . str_pad(mt_rand(1, 99999), 6, '0', STR_PAD_LEFT);
    }

    public function cancel_order()
    {

        $id = input('id');
        $order = DB::table('clt_orders')->find($id);
        if($order['status']>0){
            $data = [
                'code'=>'0',
                'msg'=>'订单已受理，不能退单',
            ];
            return json($data);
        }
        $res = DB::table('clt_orders')->where('id',$id)->update(['status' => 4]);
        if ($res){
            $data = [
                'code'=> '1',
                'msg'=>'取消成功',
            ];
        }else{
            $data = [
                'code'=>'0',
                'msg'=>'取消失败请联系管理员',
            ];
        }
        return json($data);
    }

    public function edit()
    {
        $db = db('jc_category');
        $p = $db->where('pid', 0)->select();
        $order_info = DB::table('clt_orders')->find(input('order_id/d'));
        $res = db('order_goods')->where('order_id',input('order_id/d'))->find();
        if(!$order_info){
            $data = [
                'code'=>'0',
                'msg'=>'获取信息失败',
            ];
            return json($data);
        }
        $jc_cate = $order_info['jc_cate'];
        $str = '';
        if($jc_cate) {

            $list = json_decode($order_info['jc_cate']);

            if(count($list)){

                foreach ($list as $k=>$v){

                    if($k>0){
                        $str .= '名称：'.$v->name.'&nbsp;&nbsp;数量：'.$v->num.'<br>';
                    }else{
                        $str .= '名称：'.$v->name.'&nbsp;&nbsp;数量：'.$v->num.'<br>';
                    }

                }
            }

        }
        $order_info['jc'] = $str;
        $this->assign('p', $p);
        $this->assign('info', $order_info);
        $this->assign('res', $res);

        return $this->fetch('order/order_edit');
    }

    public function edithandle()
    {

        if (request()->isPost()) {
            $data = input();

            $jc_cate = json_encode(input('jc_cate/a'));
            unset($data['jc_cate']);

            if(count($jc_cate)==0) $jc_cate = '';

            $order_id = input('order_id/d');

            $order_info = db('orders')->find($order_id);

            if(!$order_info){

                $result = [
                    'code' => '0',
                    'msg'  => '获取订单信息失败',
                ];
                return json($result);
            }

            $order = [
                'title' => $data['title'],
                'uid'   =>$_SESSION['think']['user']['id'],
                'endtime' => strtotime($data['endtime']),
                'time'=>time(),
                'company' => $data['company'],
                'address' => $data['address'],
                'r_name' => $data['r_name'],
                'r_address' => $data['r_address'],
                'r_phone' => $data['r_phone'],
                'code'   =>$data['code'],
                'chuli_type' => $data['chuli_type'],
                'receipt' => $data['receipt'],
//                'intelligence' =>json_encode($data['intelligence']),
                'subpackage' => $data['subpackage'],
                'format' => $data['format']?$data['format']:'',
                'way' => $data['way'],
                'urgent' => $data['urgent'],
                'file' => $data['file'],
                'tatchsn' => $data['tatchsn'],
                'jc_cate' => $jc_cate,
                'update_time' => time()
            ];
            foreach ($order as $k=>$v){
                if(!$v) unset($order[$k]);
            }
            $xiangmu = [
                'province' => $data['data']['province'],
                'city' => $data['data']['city'],
                'area' => $data['data']['area'],
                'caizhi' => $data['data']['caizhi'],
                'xinghao' => $data['data']['xinghao'],
                'other' => $data['data']['other'],
                'num' => $data['data']['num'],
                'content' => $data['data']['content'],
                'update_time' => time()
            ];
            foreach ($xiangmu as $k=>$v){
                if(!$v) unset($xiangmu[$k]);
            }
            db('orders')->where('id',$order_id)->update($order);
            if ($order_id){
                $good_id = db('order_goods')->where('order_id',$order_id)->update($xiangmu);
                if ($good_id){
                    $result = [
                        'code' => '1',
                        'msg'  => '信息修改成功',
                        'order_id'=>$order_info['order_Sn']
                    ];
                    return json($result);
                }
            }
        }
    }

    // 报告发布成功需要显示的页面
    public function order_success()
    {
        return $this->fetch('order/order_success');
    }

}