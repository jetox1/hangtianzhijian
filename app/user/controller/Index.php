<?php
namespace app\user\controller;
use think\Input;
class Index extends Common{
    public function _initialize(){
        parent::_initialize();
        if (!session('user.id')) {
            $this->redirect('login/index');
        }
    }
    public function index(){
        $this->assign('title','会员中心');
        $this->assign('page_name','index');
        return $this->fetch();
    }
    public function orders(){
        $this->assign('page_name','orders');
        return view();
    }
    public function collection(){
        $this->assign('page_name','collection');
        return view();
    }
    public function information(){
        $this->assign('page_name','information');
        return view();
    }

    public function estimate(){
        $this->assign('page_name','estimate');
        return view();
    }
    public function company(){
        $this->assign('page_name','company');
        return view();
    }
    public function myorders(){
        $this->assign('page_name','myorders');
        return view();
    }

}