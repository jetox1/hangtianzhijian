<?php
namespace app\user\controller;
use think\Controller;
use app\common\lib\Aes;
class Login extends Controller{
    public function _initialize(){
        if (session('user.id')) {
            $this->redirect('index/index');

        }
        ini_set("session.cookie_httponly", 1);
    }
    public function index(){
        if(request()->isPost()) {
            $jiami_data = input('datas');
            $data = json_decode(Aes::_decrypt($jiami_data),true);
            $table = db('users');
            $username = $data['username'];
            $password = $data['password'];
            if(!$username || !$password){
                return array('code'=>0,'msg'=>'请填写账号或密码');
            }
            $user = $table->where("mobile",$username)->whereOr('email',$username)->find();
            if(!$user){
                return array('code'=>0,'msg'=>'登录信息错误!');
            }
            $check_res = $this->check_login_error($user);
            if(!$check_res){
                return array('code'=>0,'msg'=>'密码输入错误次数过多，请24小时后重新登录');
            }
            if(!empty($user['salt'])){
                if(md5(md5($password).$user['salt'].$user['salt'].$user['salt']) != $user['password']){
                    //添加错误登录次数
                    $this->create_login_error($user);
                    return array('code'=>0,'msg'=>'登录信息错误!!');
                }
            }else{
                if(md5($password) != $user['password']){
                    //添加错误登录次数
                    $this->create_login_error($user);
                    return array('code'=>0,'msg'=>'登录信息错误!!!');
                }
            }
            if($user['is_lock'] == 1){
                return array('code'=>0,'msg'=>'账号异常已被锁定！！！');
            }
            $sessionUser = $table->where("id",$user['id'])->field('id,username')->find();
            //登录成功，清除登录错误记录
            $table->where("id",$user['id'])->update(['login_err_num'=>0,'unlock_time'=>0]);
            //创建一个唯一id数组作为唯一登录检测，如果没有此值就创建一个数组
            if(!cache('sessionIds')){
                //创建一个数组，将id作为key把session_id作为值存到redis
                $sessionIds = [];
                $sessionIds[$user['id']] = session_id();
                cache('sessionIds',$sessionIds);
            }else{
                //找到登录id 对应的session_id值并改变这个值
                $sessionIds = cache('sessionIds');
                $sessionIds[$user['id']] = session_id();
                cache('sessionIds',$sessionIds);
            }
            session('user',$sessionUser);
            return array('code'=>1,'msg'=>'登录成功','url' => url('index/index'));
        }else{
            $plugin = db('plugin')->where(['type'=>'login','status'=>1])->select();
            $this->assign('plugin', $plugin);
            $this->assign('title','会员登录');
            return $this->fetch();
        }
    }

    /**
     * 检测用户是否能登录
     * @param $user
     * @return bool
     */
    public function check_login_error($user){
        //错误达到10次，且未到解锁时间，不能登录
        if($user['login_err_num'] >= 10 && $user['unlock_time'] > time()){
            return false;
        }else{
            return true;
        }
    }

    /**
     * 处理用户登录错误数据
     * @param $user
     */
    private function create_login_error($user){
        $table = db('users');
        if($user['login_err_num'] < 10){
            //错误不超过10次一直加
            $table->where("id",$user['id'])->setInc('login_err_num',1);
            $table->where("id",$user['id'])->update(['unlock_time'=>time()+24*3600]);
        }else{
            //大于10次，如果过了解锁时间，重新计算
            if($user['unlock_time'] < time()){
                $table->where("id",$user['id'])->update(['login_err_num'=>1,'unlock_time'=>0]);
            }
        }
    }

    private function check($code){
        if (!captcha_check($code)) {
            return false;
        } else {
            return true;
        }
    }

    public function checkcode($code){
        if (!captcha_check($code)) {
            $data=array(
                'code' => 0,
                'msg'  => '验证码错误',
            );
        } else {
            $data=array(
                'code' => 1,
            );
        }
        return json($data);
    }
    public function reg(){
        if(request()->isPost()) {
            $jiami_data = input('datas');
            $data = json_decode(Aes::_decrypt($jiami_data),true);
            if (!captcha_check($data['vercode'])) {
                return array('code'=>0,'msg'=>'验证码错误');
            }
            if(!preg_match('/(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,30}/',$data['password'])) {
                return array('code'=>0,'msg'=>'密码为8-30个字母数字特殊字符组合');
            }
            $is_validated = 0 ;
            if(is_email($data['mobile'])){
                $is_validated = 1;
                $map['email_validated'] = 1;
                $map['email'] = $data['mobile']; //邮箱注册
            }
            if(is_mobile_phone($data['mobile'])){
                $is_validated = 1;
                $map['mobile_validated'] = 1;
                $map['mobile'] = $data['mobile']; //手机注册
            }
            if($is_validated != 1){
                return array('code'=>0,'msg'=>'请用手机号或邮箱注册');
            }
            if(!$data['username'] || !$data['password']){
                return array('code'=>-1,'msg'=>'请输入昵称或密码');
            }
            //验证两次密码是否匹配
            if($data['password'] != $data['password2']){
                return array('code'=>-1,'msg'=>'两次输入密码不一致');
            }
            //验证是否存在用户名
            if(get_user_info($data['email'],1)||get_user_info($data['mobile'],2)){
                return array('code'=>-1,'msg'=>'账号已存在');
            }
            $rand = rand(100000,999999);
            $md5str = md5($rand);
            $salt = substr($md5str,0,4);
            $map['username'] = $data['username'];
            $map['salt'] = $salt;
            $map['password'] = md5(md5($data['password']).$map['salt'].$map['salt'].$map['salt']);
            $map['reg_time'] = time();
            $map['type']  = $data['type'];
            $map['token'] = md5(time().mt_rand(1,99999));
            $id = db('users')->insertGetId($map);
            if($id === false){
                return array('code'=>-1,'msg'=>'注册失败');
            }
            $user = db('users')->field('id,username,type')->where("id", $id)->find();
            session('user',$user);
            return array('code'=>1,'msg'=>'注册成功','result'=>$user);
        }else {
            $plugin = db('plugin')->where(['type' => 'login', 'status' => 1])->select();
            $this->assign('plugin', $plugin);
            $this->assign('title', '会员注册');
            return $this->fetch();
        }
    }

    public function forget(){

        if(request()->isPost()) {
            $data = session('validate_code');
            $jiami_data = input('datas');
            $datas = json_decode(Aes::_decrypt($jiami_data),true);
            $sender = $datas['email'];
            $code = $datas['code'];
            $type = $datas['type'];
            $inValid = true;  //验证码失效
            if(!$code){
                return array('code'=>-1,'msg'=>'请输入验证码');
            }
            $data = session('validate_code');
            $timeOut = $data['time'];
            if($data['code'] != $code || $data['sender']!=$sender){
                $inValid = false;
            }
            if($type == 0){
                $data = session('validate_code');
                $timeOut = $data['time'];
                if($data['code'] != $code || $data['sender']!=$sender){
                    $inValid = false;
                }
            }else{
                $data = session('smscode');
                $timeOut = $data['time'];
                if($data['code'] != $code || $data['sender']!=$sender){
                    $inValid = false;
                }
            }
            $password = $datas['password'];
            $password2 = $datas['password2'];
            if($password != $password2){
                return array('code'=>-1,'msg'=>'两次输入密码不一致');
            }
            if(!preg_match('/(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,30}/',$password)) {
                return array('code'=>0,'msg'=>'密码为8-30个字母数字特殊字符组合');
            }
            if(empty($data)){
                return array('code'=>-1,'msg'=>'请先获取验证码');
            }elseif($timeOut < time()){
                return array('code'=>-1,'msg'=>'验证码已超时失效');
            }elseif(!$inValid) {
                return array('status'=>-1,'msg'=>'验证失败,验证码有误');
            }else{
                $data['is_check'] = 1; //标示验证通过
                session('validate_code',$data);
                $rand = rand(100000,999999);
                $md5str = md5($rand);
                $salt = substr($md5str,0,4);
                $new_data['password'] = md5(md5($password).$salt.$salt.$salt);
                $new_data['salt'] = $salt;
                $ret = db('users')->where(['email'=>$sender])->update($new_data);
                if($ret !== false){
                    return array('code'=>1,'msg'=>'密码找回成功！');
                }else{
                    return array('code'=>0,'msg'=>'密码找回失败！');
                }
            }
        }else{
            $this->assign('title','找回密码');
            return $this->fetch();
        }
    }
    /**
     * 发送邮件验证码
     * @param $sender 接收人
     * @return json
     */
    public function sendEmail(){
        if(request()->isPost()) {
            $sender = input('email');
            $sms_time_out = 180;
            //获取上一次的发送时间
            $send = session('validate_code');
            if (!empty($send) && $send['time'] > time() && $send['sender'] == $sender) {
                //在有效期范围内 相同号码不再发送
                return json(['code' => -1, 'msg' => '规定时间内,不要重复发送验证码']);
            }
            $code = mt_rand(1000, 9999);
            //检查是否邮箱格式
            if (!is_email($sender)) {
                return json(['code' => -1, 'msg' => '邮箱码格式有误']);
            }
            $send = send_email($sender, '验证码', '您好，你的验证码是：' . $code);
            if ($send) {
                $info['code'] = $code;
                $info['sender'] = $sender;
                $info['is_check'] = 0;
                $info['time'] = time() + $sms_time_out; //有效验证时间
                session('validate_code', $info);
                return json(['code' => 1, 'msg' => '验证码已发送，请注意查收'. $code]);
            }else{
                return array('code' => -1, 'msg' => '验证码发送失败,请联系管理员');
            }
        }else{
            return json(['code' => -1, 'msg' => '非法请求']);
        }
    }
    public function Sendsms(){
        $type = input('type');
        if($type == 'phone'){
            $sender = input('email');
        }else{
            $sender = input('mobile');
        }
        //检查是否手机格式
        if (!is_mobile_phone($sender)) {
            return json(['code' => -1, 'msg' => '手机号码格式有误']);
        }
        session('smscode',$sender);
        $code = $this->createSMSCode();
        $send = send_sms($sender, '【航天智检】验证码：'.$code.'，请在5秒分钟内进行操作。');
        if ($send) {
            $_SESSION['smscode'] = $code;
            $_SESSION['mobile']  = $sender;
            return json(['code' => 1, 'msg' => $send]);
        } else {
            return json(['code' => 0, 'msg' => $send]);
        }
    }
    public function createSMSCode($length = 4){
        $min = pow(10 , ($length - 1));
        $max = pow(10, $length) - 1;
        $smscode = rand($min, $max);
        session('smscode',$smscode);
        return $smscode;
    }
    public function checksmscode(){
        $smscode = input('smscode');
        $mobile = input('mobile');
        if ($smscode == $_SESSION['smscode']&&$mobile = $_SESSION['mobile']){
            return json(['code'=>'0']);
        }else{
            return json(['code'=>'1','msg'=>'短信验证码错误']);
        }

    }

    public function forget_by_mobile()
    {
        $jiami_data = input('datas');
        $data = json_decode(Aes::_decrypt($jiami_data),true);
        $mobile = $data['email'];
        $password = $data['password'];
        $password2 = $data['password2'];
        if(!preg_match('/(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,30}/',$data['password'])) {
            return array('code'=>0,'msg'=>'密码为8-30个字母数字特殊字符组合');
        }
        if(!$password || !$password2) return json(['code' => 1, 'msg' => '请输入密码']);
        if($password != $password2) return json(['code' => 1, 'msg' => '两次密码输入不一致']);
        if (!is_mobile_phone($mobile)) return json(['code' => 1, 'msg' => '手机号格式有误']);
        if($data['code'] != session('smscode')) return json(['code' => 1, 'msg' => '短信验证码有误']);
        $rand = rand(100000,999999);
        $md5str = md5($rand);
        $salt = substr($md5str,0,4);
        $new_data['password'] = md5(md5($password).$salt.$salt.$salt);
        $new_data['salt'] = $salt;
        $update_msg = db('users')->where(['mobile'=>$mobile])->update($new_data);

        if($update_msg !== false){

            return json(['code' => 1, 'msg' => '密码找回成功']);
        }else{

            return json(['code' => -1, 'msg' => '密码找回失败']);
        }

    }

    /*废弃*/
    public function doSendEmail($to,$subject='',$content=''){

        vendor('phpmailer.PHPMailerAutoload'); ////require_once vendor/phpmailer/PHPMailerAutoload.php';

        $mail = new \PHPMailer();

        $arr = db('config')->where('inc_type','smtp')->select();

        $config = convert_arr_kv($arr,'name','value');

        $mail->CharSet  = 'UTF-8'; //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码

        $mail->isSMTP();

        //Enable SMTP debugging

        // 0 = off (for production use)

        // 1 = client messages

        // 2 = client and server messages

        $mail->SMTPDebug = 1;

        //调试输出格式

        //$mail->Debugoutput = 'html';

        //smtp服务器

        $mail->Host = $config['smtp_server'];

        //端口 - likely to be 25, 465 or 587

        $mail->Port = $config['smtp_port'];



        if($mail->Port === 465) $mail->SMTPSecure = 'ssl';// 使用安全协议

        //Whether to use SMTP authentication

        $mail->SMTPAuth = true;

        //发送邮箱

        $mail->Username = $config['smtp_user'];

        //密码

        $mail->Password = $config['smtp_pwd'];

        //Set who the message is to be sent from

        $mail->setFrom($config['smtp_user'],$config['email_id']);


        //回复地址

        //$mail->addReplyTo('replyto@example.com', 'First Last');

        //接收邮件方

        if(is_array($to)){

            foreach ($to as $v){

                $mail->addAddress($v);

            }

        }else{

            $mail->addAddress($to);

        }



        $mail->isHTML(true);// send as HTML

        //标题

        $mail->Subject = $subject;

        //HTML内容转换

        $mail->msgHTML($content);

        return $mail->send();

    }
}