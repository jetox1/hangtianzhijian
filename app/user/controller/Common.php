<?php
namespace app\user\controller;
use think\Input;
use think\Controller;
class Common extends Controller{
    protected $userInfo;
    public function _initialize(){
        $this->userInfo=db('users')->alias('u')
            ->join(config('database.prefix').'user_level ul','u.level = ul.level_id','left')
            ->where(['u.id'=>session('user.id')])
            ->field('u.*,ul.level_name')
            ->find();
        $this->assign('userInfo',$this->userInfo);
        if(session('user.id')){
            //登录状态检测用户是否在其他地方登录
            if(!$this->onlyLoginCheck()){
                session('user',NULL);
                exit('<script>alert("您的账号已在其他设备登录，如非本人操作请及时修改密码");setTimeout("window.location.reload()",2000);</script>');
            }
        }
    }
    public function _empty(){
        return $this->error('空操作，返回上次访问页面中...');
    }
    //检查唯一登录
    protected function onlyLoginCheck(){
        $sessionIds = cache('sessionIds');
        $id = session('user.id');
        //存入的session_id和登录的session_id相等则返回true
        if($sessionIds[$id] == session_id()){
            return true;
        }else{
            return false;
        }
    }
    //退出登陆
    public function logout(){
        session('user',null);
        $this->redirect('login/index');
    }
}