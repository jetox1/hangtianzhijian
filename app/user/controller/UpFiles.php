<?php
namespace app\user\controller;
class UpFiles extends Common
{
    public function upImages(){
        $fileKey = array_keys(request()->file());
        // 获取表单上传文件
        $file = request()->file($fileKey['0']);
        // 移动到框架应用根目录/public/uploads/ 目录下
        $info = $file->validate(['size'=>'8388608','ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');
        if($info){
            $result['code'] = 0;
            $result['msg'] = '图片上传成功!';
            $path=str_replace('\\','/',$info->getSaveName());
            $result["src"] = '/uploads/'. $path;
            return $result;
        }else{
            // 上传失败获取错误信息
            $result['code'] =1;
            $result['msg'] = '图片上传失败!';
            return $result;
        }
    }

    public function upload(){
        if (!session('user.id')) {
            // 上传失败获取错误信息
            $result['code'] =0;
            $result['msg'] = '请先登录!';
            return $result;
        }
        $fileKey = array_keys(request()->file());
        // 获取表单上传文件
        $file = request()->file($fileKey['0']);
        // 移动到框架应用根目录/public/uploads/ 目录下
        $info = $file->validate(['size'=>'5242880','ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');
        if($info){
            $path=str_replace('\\','/',$info->getSaveName());
            $imgpath = '/uploads/'. $path;
            $res = db('users')->where("id",session('user.id'))->update(['avatar'=>$imgpath]);
            if($res){
                $result['code'] = 1;
                $result['msg'] = '图片上传成功!';
                $result["src"] = $imgpath;
                return $result;
            }else{
                // 上传失败获取错误信息
                $result['code'] =0;
                $result['msg'] = '图片上传失败!';
                return $result;
            }
        }else{
            // 上传失败获取错误信息
            $result['code'] =0;
            $result['msg'] = '图片上传失败!';
            return $result;
        }
    }
}