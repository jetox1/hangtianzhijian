<?php
namespace app\common\lib;

/**
 * aes 加密 解密类库
 * @by singwa
 * Class Aes
 * @package app\common\lib
 */
class Aes {

    /**
     * method 为AES-128-CBC时
     * @var string传入要加密的明文
     * 传入一个16字节的key
     * 传入一个16字节的初始偏移向量IV
     */
    private static $key = '2714927149271492';
    private static $iv = '2714927149271492';


    public static function _encrypt($input){
        $data = base64_encode(openssl_encrypt($input,"AES-128-CBC",substr(md5(self::$key),0,16),OPENSSL_RAW_DATA,substr(md5(self::$iv),0,16)));
        return $data;
    }

    /**
     * @return string 解密
     * @param $input 加密字符串
     * @author: ${USER}
     * @DateTime: ${DATE} ${TIME}
     */
    public static function _decrypt($input){
        $data = openssl_decrypt(base64_decode($input),"AES-128-CBC",substr(md5(self::$key),0,16),OPENSSL_RAW_DATA,substr(md5(self::$iv),0,16));

        if (!$data) {
            return false;
        }
        return $data;
    }


}