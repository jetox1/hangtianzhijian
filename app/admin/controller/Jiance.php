<?php

namespace app\admin\controller;
//use think\{Controller,Db,Input};//
use think\Controller;
use think\Db;
use think\Input;

class Jiance extends Common{
    public function index(){

        return view();
    }
    public function lists(){
        $list = db('jc_category')->select();
        $lists = get_tree($list);
        return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$lists,'rel'=>1];
    }
    public function add(){
        if(request()->isPost()){
            $data = input('post.');
            $arr = explode('-',$data['pid']);
            $map=[
                'name' => $data['name'],
                'pid'   => $arr[0],
                'desc'  =>$data['desc'],
                'jcyj'  =>$data['jcyj'],
                'status'=> $data['status'],
                'sort'  => $data['sort'],
                'type'  => $arr[1]+1
            ];
            $result = db('jc_category')->insert($map);
            if($result){
                return json(['code'=>1,'msg'=>'添加成功']);
            }else{
                return json(['code'=>0,'msg'=>'添加失败']);
            }
        }else{
            $arr = db('jc_category')->order('id')->select();
            $lists = get_tree($arr);
            $this->assign('jccategory',$lists);
            return $this->fetch();
        }
    }
    public function changestate(){
        $id=input('post.id');
        $status=input('post.status');
        if(db('jc_category')->where('id='.$id)->update(['status'=>$status])!==false){
            return ['status'=>1,'msg'=>'设置成功!'];
        }else{
            return ['status'=>0,'msg'=>'设置失败!'];
        }
    }
    public function edit(){
        if(request()->isPost()){
            $data = input('post.');
            $map['name'] = $data['name'];
            $map['status'] =$data['status'];
            $map['sort'] =$data['sort'];
            $map['jcyj'] =$data['jcyj'];
            $map['desc'] =$data['desc'];
            if (db('jc_category')->where('id',$data['id'])->update($map)){
                $code = [
                    'code'=>'1',
                    'msg' =>'更新成功'
                ];
            }else{
                $code = [
                    'code'=>'0',
                    'msg' =>'更新失败'
                ];
            }
            return json($code);
        }else{
            $id = intval(input('id'));
            $res = db('jc_category')->where('id',$id)->find();
            $this->assign('info',$res);
            return view();
        }
    }
    function del(){
        $id=input('post.id');
        $status=input('post.status');
        if(db('jc_category')->where('id='.$id)->delete()!==false){
            return ['status'=>1,'msg'=>'删除成功!'];
        }else{
            return ['status'=>0,'msg'=>'删除失败!'];
        }
    }
    public function ruleOrder(){
        $id=input('post.id');
        $sort=input('post.sort');
        if(db('jc_category')->where('id='.$id)->update(['sort'=>$sort])!==false){
            return [ 'code' => 1,'msg' => '排序成功！','url'=>url('index')];
        }else{
            return [ 'code' => 0,'msg' => '排序失败！','url'=>url('index')];
        }
    }
}