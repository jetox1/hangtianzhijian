<?php
namespace app\admin\controller;
use think\Request;
use think\Db;
use think\Controller;
class Common extends Controller
{
    protected $mod,$role,$system,$nav,$menudata,$cache_model,$categorys,$module,$moduleid,$adminRules,$HrefId;
    public function _initialize()
    {

        //判断管理员是否登录
        if (!session('aid')) {
            $this->redirect('login/index');
        }
        define('MODULE_NAME',strtolower(request()->controller()));
        define('ACTION_NAME',strtolower(request()->action()));
        //权限管理
        //当前操作权限ID
        if(session('aid')!=1){
            $this->HrefId = db('auth_rule')->where('href',MODULE_NAME.'/'.ACTION_NAME)->value('id');
            //当前管理员权限
            $map['a.admin_id'] = session('aid');
            $rules=Db::table(config('database.prefix').'admin')->alias('a')
                ->join(config('database.prefix').'auth_group ag','a.group_id = ag.group_id','left')
                ->where($map)
                ->value('ag.rules');
            $this->adminRules = explode(',',$rules);
            if($this->HrefId){
                if(!in_array($this->HrefId,$this->adminRules)){
                    $this->error('您无此操作权限','index');
                }
            }
        }
        $this->system = F('System');
        $this->categorys = F('Category');
        $this->module = F('Module');
        $this->mod = F('Mod');
        $this->role = F('Role');
        $this->cache_model=array('Module','Role','Category','Posid','Field','System');
        if(empty($this->system)){
            foreach($this->cache_model as $r){
                savecache($r);
            }
        }
    }
    //空操作
    public function _empty(){
        return $this->error('空操作，返回上次访问页面中...');
    }

    /**
     * 记录后台管理员操作记录
     * @param string $content 操作内容
     * @return bool
     */
    protected function adminlog($content){
        $request= \think\Request::instance();
        $module = strtolower($request->module());//模块名
        $controller = strtolower($request->controller());//控制器名
        $action = strtolower($request->action());//方法名
        $url = $module.'/'.$controller.'/'.$action;
        $admin_id = session('aid');
        $username = session('username');
        $data = [
            'admin_id' => $admin_id,
            'username' => $username,
            'url' => $url,
            'content' => $content,
            'ip' => $request->ip(),
            'useragent' => $request->header('user-agent'),
            'createtime' => time()
        ];
        $res = db('admin_log')->insert($data);
        if($res){
            return true;
        }else{
            return false;
        }
    }
}
