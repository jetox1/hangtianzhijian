<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Input;
use think\Request;

/**
 * Class Xuqiu
 * @package app\admin\controller
 * status
 * 0  未受理
 * 1  受理中
 * 2  已完成
 */
class Sysgl extends Common
{
    public function syssh(){
        return view();
    }
    public function ajax_list($page=null){
        $status  = input('status')?input('status'):0;
        $join = [
            ['clt_users d','s.uid=d.id'],
        ];
        $pageSize =input('limit')?input('limit'):config('pageSize');

        $data = Db::table('clt_sysrz')->alias('s')
            ->join($join)
            ->where('s.status', $status)
            ->field('s.*,d.username as uid,s.uid as sid')
            ->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();
        foreach($data['data'] as $item){
            $name = db('jc_category')->where('id',$item['catid'])->column('name');
            if($name==null){
                $item['catid'] = db('jcxiangmu')->where('id',$item['catid'])->column('title');
            }else{
                $item['catid'] = $name;
            }
            $arr[]=$item;
        }
        if ($arr) {
            $data = [
                'code' => '0',
                'msg' => '获取成功',
                'count' => $data['total'],
                'data' => $arr,
            ];

        }else{
            $data = [
                'code' => '0',
                'msg' => '获取成功',
            ];
        }
        return json($data);
    }
    public function syslist(){
            return view();
    }
    function change_category($id=null,$catid=null){
        if(request()->post()){
            $res['id'] = $id;
            $res['catid']=$catid;
            if(db('sysrz')->update($res)){
                $data['code']=1;
                $data['msg']='更新分类成功';
                $data['url']=url('syslist');
                return json($data);
            }
        }else{
            $category = db('jc_category')->where('pid',0)->select();
            $this->assign('category',$category);
            $this->assign('id',$id);
            return view();
        }

    }
    function get_category($id=null){
        $category = db('jc_category')->where('pid',$id)->select();
        $data['code']=1;
        $data['data']=$category;
        return json($data);
    }
    function get_jcxiangmu($id=null){
        $category = db('jcxiangmu')->where('catid',$id)->select();
        $data['code']=1;
        $data['data']=$category;
        return json($data);
    }
//    实验室审核
    public function change_Status(){
        $id = input('post.id');
        if (db('sysrz')->where('id',$id)->setField('status',1)){
            $sys = db('sysrz')->where('id',$id)->value('name');
            $this->adminlog('实验室资料审核：'.$sys);
            $data=[
                'code'=>1,
                'msg'=>'审核完成'
            ];
        }else{
            $data=[
                'code'=>0,
                'msg'=>'审核失败'
            ];
        }
        return json($data);
    }
    public function shiming_check(){
        $id = input('post.id');
        if (db('sysrz')->where('id',$id)->setField('is_sm',1)){
            $sys = db('sysrz')->where('id',$id)->value('name');
            $this->adminlog('实验室认证审核：'.$sys);
            $data=[
                'code'=>1,
                'msg'=>'认证审核完成'
            ];
        }else{
            $data=[
                'code'=>0,
                'msg'=>'认证审核失败'
            ];
        }
        return json($data);
    }
    public function qxshiming_check(){
        $id = input('post.id');
        if (db('sysrz')->where('id',$id)->setField('is_sm',0)){
            $sys = db('sysrz')->where('id',$id)->value('name');
            $this->adminlog('取消实验室认证审核：'.$sys);
            $data=[
                'code'=>1,
                'msg'=>'取消认证审核完成'
            ];
        }else{
            $data=[
                'code'=>0,
                'msg'=>'取消认证审核失败'
            ];
        }
        return json($data);
    }
    public function ajax_status(){
        $id = input('post.id');
        if (db('sysrz')->where('id',$id)->setField('status',0)){
            $sys = db('sysrz')->where('id',$id)->value('name');
            $this->adminlog('禁用实验室：'.$sys);
            $data=[
                'code'=>1,
                'msg'=>'禁用完成'
            ];
        }
        return json($data);
    }

    public function ajax_deletesys(){
        $id = \input('post.id');
        $sys = db('sysrz')->where('id',$id)->value('name');
        if(db('sysrz')->where('id',$id)->delete()){
            $this->adminlog('删除实验室：'.$sys);
            $data=[
                'code'=>1,
                'msg'=>'删除成功'
            ];
        }
        return json($data);
    }

    public function ajax_position(){
        $id = input('post.id');
        if (db('sysrz')->where('id',$id)->setField('position',1)){
            $sys = db('sysrz')->where('id',$id)->value('name');
            $this->adminlog('置顶实验室：'.$sys);
            $data=[
                'code'=>1,
                'msg'=>'置顶成功'
            ];
        }
        return json($data);
    }
    public function ajax_qposition(){
        $id = input('post.id');
        if (db('sysrz')->where('id',$id)->setField('position',0)){
            $sys = db('sysrz')->where('id',$id)->value('name');
            $this->adminlog('取消置顶实验室：'.$sys);
            $data=[
                'code'=>1,
                'msg'=>'取消置顶成功'
            ];
        }
        return json($data);
    }
    //实验室详情
    public function ajax_read(){
        $id = input('post.id');
        $res = db('sysrz')->where('id',$id)->find();
        $res['yyzz'] = json_decode($res['yyzz'], true);
        $res['zzry'] = json_decode($res['zzry'], true);
        if ($res){
            $data=[
                'code'=>1,
                'msg'=>'审核完成',
                'data'=>$res
            ];
        }else{
            $data=[
                'code'=>0,
                'msg'=>'审核失败'
            ];
        }
        return json($data);
    }
}
