<?php
/**
 * Created by PhpStorm.
 * User: xiaoshou1
 * Date: 2018/8/10
 * Time: 17:30
 */
namespace app\admin\controller;
class Appversion extends Common{

    public function app_list()
    {
        return view('app_list');
    }

    public function app_add()
    {
        $this->assign('title','APP添加');
        return view('app_add');
    }

    //版本列表
    public function lists()
    {
        $key = input('key');
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');

        $sql = db('app_version');

        if($key){
            $sql = $sql->where('version','like',"%$key%")->whereOr('desc','like',"%$key%");
        }
        $data = $sql->order('id desc')->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();
        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' =>$data['total'],
                'data' => $data['data'],
            ];
            return json($res);
        }
    }

    //添加版本
    public function add()
    {
        $data = input();
        if(!captcha_check($data['captcha'])){
            $result['code'] =0;
            $result['msg'] =  '验证码错误';
            return $result;
        }
        if(!$data['title']){
            $result = [
                'code' => '1',
                'msg'  => '版本号不能为空',
            ];
            return json($result);
        }
        if(!$data['down_url']){
            $result = [
                'code' => '1',
                'msg'  => '安装包不能为空',
            ];
            return json($result);
        }
        $insert_msg = db('app_version')->insertGetId([
            'version' => $data['title'],
            'desc' => $data['desc'],
            'down_url' => $data['down_url'],
            'add_time' => date('Y-m-d H:i:s'),
        ]);
        if($insert_msg){
            $result = [
                'code' => '1',
                'msg'  => '添加成功',
            ];
            return json($result);
        }else{
            $result = [
                'code' => '0',
                'msg'  => '添加失败',
            ];
            return json($result);
        }
    }

    //删除版本
    public function del()
    {
        $info = db('app_version')->find(input('id/d'));
        if(!$info){
            $result = [
                'code' => '1',
                'msg'  => '获取版本信息失败',
            ];
            return json($result);
        }
        $del_msg = db('app_version')->where('id',input('id/d'))->delete();
        if($del_msg){
            $result = [
                'code' => '1',
                'msg'  => '删除成功',
            ];
            return json($result);
        }else{
            $result = [
                'code' => '1',
                'msg'  => '删除失败',
            ];
            return json($result);
        }
    }
}