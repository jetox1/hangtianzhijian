<?php
namespace app\admin\controller;
use think\Controller;
use app\admin\model\Admin;
use app\common\lib\Aes;
class Login extends Controller
{
    public function _initialize(){
        if (session('aid')) {
            $this->redirect('index/index');
        }
    }
    private $cache_model,$system;
    public function index(){
        if(request()->isPost()) {
            $admin = new Admin();
            $jiami_data = input('datas');
            $data = json_decode(Aes::_decrypt($jiami_data),true);
            if(!$this->check($data['captcha'])){
                $this->adminlog('管理员登录失败：验证码错误',$data['username']);
                return json(array('code' => 0, 'msg' => '验证码错误'));
            }
            $num = $admin->login($data);
            if($num == 1){
                $this->adminlog('管理员登录');
                return json(['code' => 1, 'msg' => '登录成功!', 'url' => url('index/index')]);
            }else {
                $this->adminlog('管理员登录失败：用户名或者密码错误',$data['username']);
                return json(array('code' => 0, 'msg' => '用户名或者密码错误，重新输入!'));
            }
        }else{
            $this->cache_model=array('Module','Role','Category','Posid','Field','System');
            $this->system = F('System');
            if(empty($this->system)){
                foreach($this->cache_model as $r){
                    savecache($r);
                }
            }
            return $this->fetch();
        }
    }
    public function check($code){
       return captcha_check($code);
    }
    /**
     * 记录后台管理员操作记录
     * @param string $content 操作内容
     * @param string $username 用户名称
     * @return bool
     */
    private function adminlog($content,$username = ''){
        $request= \think\Request::instance();
        $module = strtolower($request->module());//模块名
        $controller = strtolower($request->controller());//控制器名
        $action = strtolower($request->action());//方法名
        $url = $module.'/'.$controller.'/'.$action;
        $admin_id = session('aid') ? session('aid') : 0;
        $username = session('username') ? session('username') : $username;
        $data = [
            'admin_id' => $admin_id,
            'username' => $username,
            'url' => $url,
            'content' => $content,
            'ip' => $request->ip(),
            'useragent' => $request->header('user-agent'),
            'createtime' => time()
        ];
        $res = db('admin_log')->insert($data);
        if($res){
            return true;
        }else{
            return false;
        }
    }
}