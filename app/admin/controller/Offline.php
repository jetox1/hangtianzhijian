<?php
/**
 * Created by WY
 * Date: 2018/04/26
 * Time: 8:55
 */
namespace app\admin\controller;
use think\Db;
use think\Request;
use clt\Form;
class Offline extends  Common{
    function index(){
        return view();
    }
    function lists(){
        $keyword=trim(input('post.key'));
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');

        $sql = db('offline_orders');
        if(!empty($keyword) ){
            $sql = $sql->where('title','like',"%$keyword%")->whereOr('tatchsn','like',"%$keyword%")
                ->whereOr('reportsn','like',"%$keyword%");
        }
        $data = $sql->order('id','desc')
            ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
            ->toArray();
        foreach ($data['data'] as $k ){
            $k['addtime'] = date('Y-m-d H:i:s',$k['addtime']);
            $lists[]=$k;
        }
        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' => $data['total'],
                'data' => $lists,
            ];
            return json($res);
        }
    }
    //受理中
    function lists_ing(){
        $keyword=trim(input('post.key'));
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $sql = db('offline_orders');
        if(!empty($keyword) ){
            $sql = $sql->where('title','like',"%$keyword%")->whereOr('tatchsn','like',"%$keyword%")
                ->whereOr('reportsn','like',"%$keyword%");
        }
        $data = $sql
            ->where('status',1)
            ->order('id','desc')
            ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
            ->toArray();
        foreach ($data['data'] as $k ){
            $k['addtime'] = date('Y-m-d H:i:s',$k['addtime']);
            $lists[]=$k;
        }
        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' => $data['total'],
                'data' => $lists,
            ];
            return json($res);
        }
    }
    //已完成
    function lists_over(){
        $keyword=trim(input('post.key'));
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $sql = db('offline_orders');
        if(!empty($keyword) ){
            $sql = $sql->where('title','like',"%$keyword%")->whereOr('tatchsn','like',"%$keyword%")
                ->whereOr('reportsn','like',"%$keyword%");
        }
        $data = $sql
            ->where('status',2)
            ->order('id','desc')
            ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
            ->toArray();
        foreach ($data['data'] as $k ){
            $k['addtime'] = date('Y-m-d H:i:s',$k['addtime']);
            $lists[]=$k;
        }
        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' => $data['total'],
                'data' => $lists,
            ];
            return json($res);
        }
    }
    function add(){
        if ($this->request->post()){
            $data = input('post.');
            $data['addtime']=time();
            if (db('offline_orders')->insert($data)){
                $res = [
                    'code' => '1',
                    'msg' => '新增成功',
                    'url' => url('index')
                ];
                return json($res);
            }
        }else{
            $this->assign ( 'title', '添加内容' );
            return view();
        }
    }
    function edit($id = null){
        $id = input('id');
        $request = Request::instance();
        $info = db('offline_orders')->where('id',$id)->find();
        $this->assign ('info',$info);
        $this->assign ('title','编辑内容');
        return view();
    }
    function edithandle(){
        $data = input('post.');
        if (db('offline_orders')->update($data)){
            $code = [
                'code'=>'1',
                'msg' =>'更新成功',
                'url' => url('index')
            ];
        }else{
            $code = [
                'code'=>'0',
                'msg' =>'更新失败'
            ];
        }
        return json($code);
    }
    function del($id=null){
        if (db('offline_orders')->where('id',$id)->delete()){
            $code = [
                'code'=>'1',
                'msg' =>'删除成功'
            ];
        }else{
            $code = [
                'code'=>'0',
                'msg' =>'删除失败'
            ];
        }
        return json($code);
    }
    function jindulist(){
        $id = input('id');
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $data = Db::table('clt_order_jindu')
            ->where('offline_order_id',$id)
            ->order('id desc')
            ->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();
        if ($data){
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' => $data['total'],
                'data' => $data['data'],
            ];

        }else{
            $res = [
                'code' => '0',
                'msg' => '获取失败',
                'count' => $data['total'],
                'data' => $data['data'],
            ];
        }
        return json($res);
    }
    public function addjindu(){
        if (request()->isPost()) {
            $data['offline_order_id'] = input('order_id');
            $data['desc'] = input('desc');
            $data['status'] = input('status');
            $data['file'] = input('file');
            $data['time'] = date('Y-m-d h:i:s',time());
            $order_id = db('order_jindu')->insert($data);
            //修改线下订单状态
            $status = 0;
            if(input('status') == '订单已受理') $status = 1;
            if(input('status') == '试验已完成') $status = 2;
            db('offline_orders')->where('id',input('order_id'))->update(['status' => $status]);
            if ($order_id){
                $result = [
                    'code' => '1',
                    'msg'  => '发布成功',
                ];
            }else{
                $result = [
                    'code' => '1',
                    'msg'  => '发布成功',
                ];
            }
            return json($result);
        }
    }

    public function del_jindu()
    {

        $del_msg = Db::table('clt_order_jindu')->where('id', input('id/d'))->delete();

        if($del_msg){
            $data = [
                'code' => 1,
                'msg' => '删除成功',
            ];
        }else{
            $data = [
                'code' => 0,
                'msg' => '删除失败',
            ];
        }
        return json($data);
    }

}