<?php
/**
 * Created by PhpStorm.
 * User: xiaoshou1
 * Date: 2018/8/10
 * Time: 17:30
 */
namespace app\admin\controller;
class Feedback extends Common{

    public function feedback_list()
    {
        return view();
    }

    //版本列表
    public function lists()
    {
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $key = input('key');
        $sql = db('feedback');
        if($key) $sql = $sql->where('title',"like","%$key%")->whereOr('desc','like',"%$key%");
        $data = $sql->order('id desc')->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();
        $feedbacks = $data['data'];
        foreach ($feedbacks as $k=>$v){
            $feedbacks[$k]['add_time'] = date('Y-m-d H:i:s',$v['add_time']);
            $user_name = db('users')->where('id',$v['user_id'])->value('username');
            $feedbacks[$k]['user_name'] = $user_name;
        }
        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' =>$data['total'],
                'data' => $feedbacks,
            ];
            return json($res);
        }
    }

    //删除
    public function del()
    {
        $info = db('feedback')->find(input('id/d'));
        if(!$info){
            $result = [
                'code' => '1',
                'msg'  => '获取反馈信息失败',
            ];
            return json($result);
        }
        $del_msg = db('feedback')->where('id',input('id/d'))->delete();
        if($del_msg){
            $result = [
                'code' => '1',
                'msg'  => '删除成功',
            ];
            return json($result);
        }else{
            $result = [
                'code' => '1',
                'msg'  => '删除失败',
            ];
            return json($result);
        }
    }

    // 反馈审核
    public function ajax_position()
    {

        $id = input('id/d');
        $status = 1;

        $feedback = db('feedback')->find($id);

        $user_name = db('users')->where('id',$feedback['user_id'])->value('username');

        if(!$feedback){
            $result = [
                'code' => '1',
                'msg'  => '获取反馈信息失败',
            ];
            return json($result);
        }

        $update_msg = db('feedback')->where('id',$id)->update(['state'=>$status]);

        if($update_msg){

            //发布审核成功信息
            if($status){

                db('message')->insertGetId([
                    'title' => '您的反馈已被审核',
                    'addtime' => time(),
                    'content' => '"'.$feedback['title'].'"'.'反馈审核通过',
                    'name' => $user_name,
                    'uid' => isset($feedback['user_id'])?$feedback['user_id']:0
                ]);
            }

            $result = [
                'code' => 1,
                'msg'  => '操作成功',
            ];
            return json($result);
        }else{

            $result = [
                'code' => 0,
                'msg'  => '操作失败',
            ];
            return json($result);
        }

    }
}