<?php
/**
 * Created by PhpStorm.
 * User: xiaoshou1
 * Date: 2018/8/10
 * Time: 17:30
 */
namespace app\admin\controller;
class Topic extends Common{

    public function topic_list()
    {
        return view();
    }

    //版本列表
    public function lists()
    {
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $data = db('topic')->order('id desc')->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();
        $topics = $data['data'];
        foreach ($topics as $k=>$v){
            $topics[$k]['add_time'] = date('Y-m-d H:i:s',$v['add_time']);
        }
        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' =>$data['total'],
                'data' => $topics,
            ];
            return json($res);
        }
    }

    //删除
    public function del()
    {
        $info = db('topic')->find(input('id/d'));
        if(!$info){
            $result = [
                'code' => '1',
                'msg'  => '获取帖子信息失败',
            ];
            return json($result);
        }
        $del_msg = db('topic')->where('id',input('id/d'))->delete();
        if($del_msg){
            $result = [
                'code' => '1',
                'msg'  => '删除成功',
            ];
            return json($result);
        }else{
            $result = [
                'code' => '1',
                'msg'  => '删除失败',
            ];
            return json($result);
        }
    }

    public function ajax_position()
    {

        $id = input('id/d');
        $status = input('status/d');

        $topic = db('topic')->find($id);

        $user_name = db('users')->where('id',$topic['uid'])->value('username');

        if(!$topic){
            $result = [
                'code' => '1',
                'msg'  => '获取帖子信息失败',
            ];
            return json($result);
        }

        $update_msg = db('topic')->where('id',$id)->update(['status'=>$status]);

        if($update_msg){

            //发布审核成功信息
            if($status){

                db('message')->insertGetId([
                    'title' => '您的帖子已被审核',
                    'addtime' => time(),
                    'content' => '"'.$topic['content'].'"'.'帖子审核通过',
                    'name' => $user_name,
                    'uid' => isset($topic['uid'])?$topic['uid']:0
                ]);
            }

            $result = [
                'code' => 1,
                'msg'  => '操作成功',
            ];
            return json($result);
        }else{

            $result = [
                'code' => 0,
                'msg'  => '操作失败',
            ];
            return json($result);
        }

    }
}