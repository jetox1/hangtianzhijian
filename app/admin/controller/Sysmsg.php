<?php
/**
 * Created by PhpStorm.
 * User: xiaoshou1
 * Date: 2018/8/10
 * Time: 17:30
 */
namespace app\admin\controller;
class Sysmsg extends Common{

    public function sysmsg_list()
    {
        return view();
    }

    public function sysmsg_add()
    {

        $this->assign('title','消息编辑');

        $info = db('message')->where('message_id',input('id'))->find();

        $this->assign('info',$info);

        return view();
    }

    //系统消息列表
    public function lists()
    {
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $key = input('key');
        $sql = db('message')->where('uid',0);

        if($key) $sql = $sql->where('title','like',"%$key%")->whereOr('content','like',"%$key%");
        $data = $sql->order('message_id desc')->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();

        $data_source = $data['data'];

        foreach ($data_source as $k=>$v){
            $data_source[$k]['addtime'] = date('Y-m-d H:i:s',$v['addtime']);
        }

        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' =>$data['total'],
                'data' => $data_source,
            ];
            return json($res);
        }
    }

    //添加系统消息
    public function add()
    {
        $data = input();
        if(!$data['title']){
            $result = [
                'code' => '1',
                'msg'  => '消息标题不能为空',
            ];
            return json($result);
        }
        if(!$data['content']){
            $result = [
                'code' => '1',
                'msg'  => '消息内容不能为空',
            ];
            return json($result);
        }
        if(input('id/d')){


            $insert_msg = db('message')->where('message_id',input('id/d'))->update([

                'title' => $data['title'],
                'content' => $data['content']

            ]);

            if($insert_msg){
                $result = [
                    'code' => '1',
                    'msg'  => '修改成功',
                ];
                return json($result);
            }else{
                $result = [
                    'code' => '1',
                    'msg'  => '修改失败',
                ];
                return json($result);
            }



        }else{

            $insert_msg = db('message')->insertGetId([

                'title' => $data['title'],
                'addtime' => time(),
                'content' => $data['content'],
                'name' => '系统'

            ]);

            if($insert_msg){
                $result = [
                    'code' => '1',
                    'msg'  => '添加成功',
                ];
                return json($result);
            }else{
                $result = [
                    'code' => '1',
                    'msg'  => '添加失败',
                ];
                return json($result);
            }

        }


    }

    //删除版本
    public function del()
    {
        $info = db('message')->where('message_id',input('id/d'))->find();
        if(!$info){
            $result = [
                'code' => '1',
                'msg'  => '获取系统消息失败',
            ];
            return json($result);
        }
        $del_msg = db('message')->where('message_id',input('id/d'))->delete();
        if($del_msg){
            $result = [
                'code' => '1',
                'msg'  => '删除成功',
            ];
            return json($result);
        }else{
            $result = [
                'code' => '1',
                'msg'  => '删除失败',
            ];
            return json($result);
        }
    }

}