<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Input;

/**
 * 测试圈+论坛
 * Class Community
 * @package app\admin\controller
 * status
 * 1    审核中
 * 2    通过
 * 3    驳回
 */
class Community extends Common
{
    public function index()
    {
        return view();
    }

    function lists(){
        $keyword=input('post.key');
        $type = input('post.type');
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');
        if(!empty($keyword) ){
            $map['real_name']=array('like','%'.$keyword.'%');
        }
        if($type){
            $map['type'] = $type;
        }
        $data = db('community')
            ->where($map)
            ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
            ->toArray();
        $lists = array();
        foreach ($data['data'] as $k ){
            $user = db('users')->where('id',$k['user_id'])->find();
            $k['user'] = $user;
            $k['add_time'] = date('Y-m-d H:i:s',$k['add_time']);
            $k['status_cn'] = '显示';
            $k['reply'] = db('community_reply')->where('cid',$k['id'])->count();
            if($k['status'] == 2){
                $k['status_cn'] = '已屏蔽';
            }
            $lists[]=$k;
        }
        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' => $data['total'],
                'data' => $lists,
            ];
            return json($res);
        }
    }
    public function listorder(){
        $data = input('post.');
        db('community')->update($data);
        $result = ['msg' => '排序成功！','code' => 1];
        return $result;
    }
    function changefield($id=null,$field=null,$value = null){
        db('community')->where('id',$id)->setField($field,$value);
        $result = ['msg' => '操作成功！','code' => 1];
        return $result;
    }
    function add(){
        if ($this->request->post()){
            $data = input('post.');
            $data['add_time']=time();
            if (db('expert')->insert($data)){
                $res = [
                    'code' => '1',
                    'msg' => '新增成功',
                ];
                return json($res);
            }
        }else{
//            $list = db('expert')->select();
//            $category = get_tree($list);
//            $this->assign('category',$category);
            return view();
        }
    }
    function edit($id = null){
        $id = input('id');
        $request = Request::instance();
        $info = db('community')->where('id',$id)->find();

//        $list = db('jc_category')->select();
//        $category = get_tree($list);

//        $this->assign('category',$category);
        $this->assign ('info', $info );
        $this->assign ( 'title', '编辑内容' );
        return view();
    }

    function edithandle(){
        $data = input('post.');
//        $data['updatetime']=time();
        if (db('community')->update($data)){
            $code = [
                'code'=>'1',
                'msg' =>'更新成功'
            ];
        }else{
            $code = [
                'code'=>'0',
                'msg' =>'更新失败'
            ];
        }
        return json($code);
    }
    function del($id=null){
        if (db('community')->where('id',$id)->delete()){
            $code = [
                'code'=>'1',
                'msg' =>'删除成功'
            ];
        }else{
            $code = [
                'code'=>'0',
                'msg' =>'删除失败'
            ];
        }
        return json($code);
    }


}