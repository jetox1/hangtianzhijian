<?php
namespace app\admin\controller;
use think\Db;
use think\Request;
use clt\Leftnav;
use app\admin\model\System as SysModel;
class System extends Common
{
    /********************************站点管理*******************************/
    //站点设置
    public function system($sys_id=1){
        $table = db('system');
        if(request()->isPost()) {
            $datas = input('post.');
            if($table->where('id',1)->update($datas)!==false) {
                savecache('System');
                return json(['code' => 1, 'msg' => '站点设置保存成功!', 'url' => url('system/system')]);
            } else {
                return json(array('code' => 0, 'msg' =>'站点设置保存失败！'));
            }
        }else{
            $system = $table->field('id,name,url,title,key,des,bah,copyright,ads,tel,email,logo')->find($sys_id);
            $this->assign('system', $system);
            return $this->fetch();
        }
    }
    public function email(){
        $table = db('config');
        if(request()->isPost()) {
            $datas = input('post.');
            foreach ($datas as $k=>$v){
                $table->where(['name'=>$k,'inc_type'=>'smtp'])->update(['value'=>$v]);
            }
            return json(['code' => 1, 'msg' => '邮箱设置成功!', 'url' => url('system/email')]);
        }else{
            $smtp = $table->where(['inc_type'=>'smtp'])->select();
            $info = convert_arr_kv($smtp,'name','value');
            $this->assign('info', $info);
            return $this->fetch();
        }
    }
    public function sms(){
        $table = db('config');
        if(request()->isPost()) {
            $datas = input('post.');
            foreach ($datas as $k=>$v){
                $table->where(['name'=>$k,'inc_type'=>'sms'])->update(['value'=>$v]);
            }
            return json(['code' => 1, 'msg' => '短信设置成功!', 'url' => url('system/sms')]);
        }else{
            $smtp = $table->where(['inc_type'=>'sms'])->select();
            $info = convert_arr_kv($smtp,'name','value');
            $this->assign('info', $info);
            return $this->fetch();
        }
    }
    public function trySend(){
        $sender = input('email');
        //检查是否邮箱格式
        if (!is_email($sender)) {
            return json(['code' => -1, 'msg' => '测试邮箱码格式有误']);
        }
        $send = $this->doSendEmail($sender, '测试邮件', '您好！这是一封来自'.$this->system['name'].'的测试邮件！');
        if ($send) {
            return json(['code' => 1, 'msg' => '邮件发送成功！']);
        } else {
            return json(['code' => -1, 'msg' => '邮件发送失败！']);
        }
    }
    public function trySendsms(){
        $sender = input('mobile');
        //检查是否邮箱格式
        if (!is_mobile_phone($sender)) {
            return json(['code' => -1, 'msg' => '测试手机号码格式有误']);
        }
        $send = send_sms($sender, '【帮交社保网】验证码：556556，请在5秒分钟内进行操作。');
        if ($send) {
            return json(['code' => 1, 'msg' => $send]);
        } else {
            return json(['code' => -1, 'msg' => $send]);
        }
    }

    public function doSendEmail($to,$subject='',$content=''){

        vendor('phpmailer.PHPMailerAutoload'); ////require_once vendor/phpmailer/PHPMailerAutoload.php';

        $mail = new \PHPMailer();

        $arr = db('config')->where('inc_type','smtp')->select();

        $config = convert_arr_kv($arr,'name','value');

        $mail->CharSet  = 'UTF-8'; //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码

        $mail->isSMTP();

        //Enable SMTP debugging

        // 0 = off (for production use)

        // 1 = client messages

        // 2 = client and server messages

        $mail->SMTPDebug = 1;

        //调试输出格式

        //$mail->Debugoutput = 'html';

        //smtp服务器

        $mail->Host = $config['smtp_server'];

        //端口 - likely to be 25, 465 or 587

        $mail->Port = $config['smtp_port'];



        if($mail->Port === 465) $mail->SMTPSecure = 'ssl';// 使用安全协议

        //Whether to use SMTP authentication

        $mail->SMTPAuth = true;

        //发送邮箱

        $mail->Username = $config['smtp_user'];

        //密码

        $mail->Password = $config['smtp_pwd'];

        //Set who the message is to be sent from

        $mail->setFrom($config['smtp_user'],$config['email_id']);


        //回复地址

        //$mail->addReplyTo('replyto@example.com', 'First Last');

        //接收邮件方

        if(is_array($to)){

            foreach ($to as $v){

                $mail->addAddress($v);

            }

        }else{

            $mail->addAddress($to);

        }



        $mail->isHTML(true);// send as HTML

        //标题

        $mail->Subject = $subject;

        //HTML内容转换

        $mail->msgHTML($content);


        return $mail->send();

    }

}
