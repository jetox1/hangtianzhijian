<?php
/**
 * Created by PhpStorm.
 * User: szc80
 * Date: 2018/3/4
 * Time: 6:52
 */
namespace app\admin\controller;
use think\Db;
use think\Request;
use clt\Form;
class Xiangmu extends  Common{
    function index(){
        return view();
    }
    function lists(){
        $keyword=input('post.key');
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');
        if(!empty($keyword) ){
            $map['title']=array('like','%'.$keyword.'%');
        }
        $data = db('jcxiangmu')
            ->where($map)
            ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
            ->toArray();
        foreach ($data['data'] as $k ){
            $k['createtime'] = date('Y-m-d H:i:s',$k['createtime']);
            $k['catid'] = $this->get_jccategory($k['catid']);
            $lists[]=$k;
        }
        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' => $data['total'],
                'data' => $lists,
            ];
            return json($res);
        }
    }
    public function listorder(){
        $data = input('post.');
        db('jcxiangmu')->update($data);
        $result = ['msg' => '排序成功！','code' => 1];
        return $result;
    }
    function changefield($id=null,$field=null,$value = null){
        db('jcxiangmu')->where('id',$id)->setField($field,$value);
        $result = ['msg' => '操作成功！','code' => 1];
        return $result;
    }
    function add(){
        if ($this->request->post()){
            $data = input('post.');
            $data['createtime']=time();
            if (db('jcxiangmu')->insert($data)){
                $res = [
                    'code' => '1',
                    'msg' => '新增成功',
                ];
                return json($res);
            }
        }else{
            $list = db('jc_category')->select();
            $category = get_tree($list);

            $this->assign('category',$category);
            return view();
        }
    }
    function edit($id = null){
        $id = input('id');
        $request = Request::instance();
        $info = db('jcxiangmu')->where('id',$id)->find();

        $list = db('jc_category')->select();
        $category = get_tree($list);

        $this->assign('category',$category);
        $this->assign ('info', $info );
        $this->assign ( 'title', '编辑内容' );
        return view();
    }
    function edithandle(){
        $data = input('post.');
        $data['updatetime']=time();
        if (db('jcxiangmu')->update($data)){
            $code = [
                'code'=>'1',
                'msg' =>'更新成功'
            ];
        }else{
            $code = [
                'code'=>'0',
                'msg' =>'更新失败'
            ];
        }
        return json($code);
    }
    function del($id=null){
        if (db('jcxiangmu')->where('id',$id)->delete()){
            $code = [
                'code'=>'1',
                'msg' =>'删除成功'
            ];
        }else{
            $code = [
                'code'=>'0',
                'msg' =>'删除失败'
            ];
        }
        return json($code);
    }
    function get_jccategory($catid=null){
        $catename = db('jc_category')->where('id',$catid)->column('name');
        return $catename;
    }
}