<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Input;

/**
 * Class Xuqiu
 * @package app\admin\controller
 * status
 * 0  未受理
 * 1  受理中(平台接单)
 * 2  已完成
 * 3  拒绝受理
 * 4 用户取消订单
 * 5 已支付
 * 6 已公开
 * 7 已接单(实验室接单)
 */
class Xuqiu extends Common
{
    public function index()
    {
        return view();
    }

    public function ajax_order()
    {
        $type = input('type')?input('type'):0;
        if($type==4) $type = 6;
        if($type==5) $type = 7;
        if($type==3) $type = 5;

        $keyword=trim(input('key'));

        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $sql = db('orders');

        if(!empty($keyword) ){
            $sql = $sql->where('title','like',"%$keyword%")->whereOr('tatchsn','like',"%$keyword%")
                ->whereOr('order_Sn','like',"%$keyword%")->whereOr('reportsn','like',"%$keyword%");
        }

        if($type == 7){
            $sql = $sql->where('apply_refund',1);
        }else{
            $sql = $sql->where('apply_refund',0)->where('status', $type);
        }
        $data = $sql->order('id desc')->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();


        foreach($data['data'] as $v){
            $v['chuli_type']=get_chuli_type($v['chuli_type']);
            $v['receipt']=get_receipt($v['receipt']);
            $v['subpackage']=get_subpackage($v['subpackage']);
            $v['format']=get_format($v['format']);
            $v['way']=get_way($v['way']);
            $v['urgent']=get_urgent($v['urgent']);
            $v['status']=get_status($v['status']);
            $v['endtime']=date('Y-m-d',$v['endtime']);
            $item[] = $v;
        }
        if ($data) {
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' =>$data['total'],
                'data' => $item,
            ];
            return json($res);
        }
    }

    function show(){
        $id = input('id');
        $data = db('orders')->where('id',$id)->find();
        $res = db('order_goods')->where('order_id',$id)->find();
        $data['chuli_type']=get_chuli_type($data['chuli_type']);
        $data['receipt']=get_receipt($data['receipt']);
        $data['intelligence']=json_decode($data['intelligence']);
        $data['subpackage']=get_subpackage($data['subpackage']);
        $data['format']=get_format($data['format']);
        $data['way']=get_way($data['way']);
        $data['urgent']=get_urgent($data['urgent']);
        $data['caizhi'] = $res['caizhi'];
        $data['xinghao'] = $res['xinghao'];
        $data['other'] = $res['other'];
        $data['content'] = $res['content'];
        $data['num'] = $res['num'];

        $jc_cate = $data['jc_cate'];
        $str = '';
        if($jc_cate) {

            $list = json_decode($data['jc_cate']);

            if(count($list)){

                foreach ($list as $k=>$v){

                    if($k>0){
                        $str .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名称：'.$v->name.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数量：'.$v->num.'<br>';
                    }else{
                        $str .= '名称：'.$v->name.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;数量：'.$v->num.'<br>';
                    }

                }
            }

        }
        $data['jc'] = $str;
        if ($data){
            return json(['code'=>1,'msg'=>'显示成功','data'=>$data]);
        }
    }
    function ajax_order_show(){
        $id = input('id');
        $res = Db::table('clt_order_goods')
            ->where('order_id',$id)
            ->select();
        foreach($res as $item){
            $order_sn = db('orders')->where('id',$item['order_id'])->column('order_Sn');
            $item['order_id'] = $order_sn[0];
            $item['province'] = get_Category_name($item['province']);
            $item['city'] = get_Category_name($item['city']);
            $item['area'] = get_xiangmu_name($item['area']);
            $data[] = $item;
        }
        if ($data){
            $data = [
                'code' => '0',
                'msg' => '获取成功',
                'count' => count($data),
                'data' => $data,
            ];

        }else{
            $data = [
                'code' => '0',
                'msg' => '获取失败',
                'count' => count($data),
                'data' => $data,
            ];
        }
        return json($data);
    }
    function addjindu()
    {
        if (request()->isPost()) {

            $order = db('orders')->find(input('order_id/d'));

            if(input('status') == '试验已完成' || input('status') == '报告已上传'){

                db('orders')->where('id',input('order_id'))->setField('status',2);
            }else{

                if($order['status'] == 0){

                    db('orders')->where('id',input('order_id'))->setField('status',1);
                }

            }

            $data['order_id'] = input('order_id');
            $data['desc'] = input('desc');
            $data['status'] = input('status');
            $data['file'] = input('file');
            $data['time'] = date('Y-m-d h:i:s',time());
            $order_id = db('order_jindu')->insert($data);
            if ($order_id){

                $result = [
                    'code' => '1',
                    'msg'  => '发布成功',
                ];
            }else{
                $result = [
                    'code' => '1',
                    'msg'  => '发布成功',
                ];
            }
            return json($result);
        }
    }
    function waibao($id=null,$user_id=null){
        $data['order_id'] = intval($id);
        $data['user_id'] = $user_id;
        $id = db('user_order')->insert($data);
        if ($id){
            db('orders')->update(['id'=>$data['order_id'],'status'=>1]);
            $data=array(
                'code' => 0,
                'msg'  => '外包成功',
            );
        }else{
            $data=array(
                'code' => 0,
                'msg'  => '外包失败',
            );
        }

        return json($data);
    }

    function shouli($id = '',$info = '',$status = '',$price='',$time ='',$phone='',$addr=''){
        $data['order_id'] =$id;

        $status = intval($status);

        if ($status ==3){
            db('orders')->where('id',$data['order_id'])->update(['status'=>$status,'info'=>$info]);
            $data = array(
              'code'=>0,
              'msg'=>'已拒绝',
            );
        }elseif($status = 1){
            db('orders')->where('id',$data['order_id'])->update(['status'=>$status,'price'=>$price,'sytime'=>$time,'sysmobile'=>$phone,'sysaddr'=>$addr]);
            $data = array(
                'code'=>0,
                'msg'=>'已受理',
            );
        }
        return json($data);
    }

    function gongkai($id=null){

        $order_id = intval($id);
        $update_msg = db('orders')->where('id', $order_id)->update(['status'=>6]);
        if($update_msg){
            $data=array(
                'code' => 1,
                'msg'  => '公开成功',
            );
        }else{

            $data=array(
                'code' => 0,
                'msg'  => '公开失败',
            );
        }
        return json($data);

    }

    function delete_xuqiu($id=null){
        $order_id = intval($id);
        $update_msg = db('orders')->where('id', $order_id)->delete();
        if($update_msg){
            $data=array(
                'code' => 1,
                'msg'  => '删除成功',
            );
        }else{

            $data=array(
                'code' => 0,
                'msg'  => '删除失败',
            );
        }
        return json($data);
    }

    function jindulist(){
        $id = input('id');
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $data = Db::table('clt_order_jindu')
            ->where('order_id',$id)
            ->order('id desc')
            ->paginate(array('list_rows'=>$pageSize,'page'=>$page))->toArray();

        if ($data){
            $res = [
                'code' => '0',
                'msg' => '获取成功',
                'count' => $data['total'],
                'data' => $data['data'],
            ];

        }else{
            $res = [
                'code' => '0',
                'msg' => '获取失败',
                'count' => $data['total'],
                'data' => $data['data'],
            ];
        }
        return json($res);
    }

    public function del_jindu()
    {

        $del_msg = Db::table('clt_order_jindu')->where('id', input('id/d'))->delete();

        if($del_msg){
            $data = [
                'code' => 1,
                'msg' => '删除成功',
            ];
        }else{
            $data = [
                'code' => 0,
                'msg' => '删除失败',
            ];
        }
        return json($data);
    }

    public function through_apply($id=null)
    {


        $order_id = intval($id);
        $update_msg = db('orders')->where('id', $order_id)->update(['status' => 4,'apply_refund' => 0]);
        if($update_msg){
            db('user_order')->where('order_id',$order_id)->delete();
            $data=array(
                'code' => 1,
                'msg'  => '成功',
            );
        }else{

            $data=array(
                'code' => 0,
                'msg'  => '失败',
            );
        }
        return json($data);
    }

    public function refuse_apply($id=null)
    {


        $order_id = intval($id);
        $update_msg = db('orders')->where('id', $order_id)->update(['apply_refund' => 0]);
        if($update_msg){
            $data=array(
                'code' => 1,
                'msg'  => '成功',
            );
        }else{

            $data=array(
                'code' => 0,
                'msg'  => '失败',
            );
        }
        return json($data);
    }

    function edit($id = null){
        $id = input('id');
        $info = db('orders')->where('id',$id)->find();
        $this->assign ('info',$info);
        $this->assign ('title','编辑内容');
        return view();
    }

    function edithandle(){
        $data = input('post.');
        if (db('orders')->update($data)){
            $code = [
                'code'=>'1',
                'msg' =>'更新成功',
                'url' => url('index')
            ];
        }else{
            $code = [
                'code'=>'0',
                'msg' =>'更新失败'
            ];
        }
        return json($code);
    }

}