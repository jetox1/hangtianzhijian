<?php
namespace app\api\controller;
use think\controller;

class order{
    function add()
    {          //处理接口提交数据
        if (request()->isPost()) {
            $data = input();
            $lng = $data['lng'];
            $lat = $data['lat'];
            $order = [
                'title' => $data['title'],
                'uid' => $data['user_id'],
                'endtime' => time($data['endtime']),
                'time' => time(),
                'company' => $data['company'],
                'address' => $data['address'],
                'r_name' => $data['r_name'],
                'r_address' => $data['r_address'],
                'r_phone' => $data['r_phone'],
                'code' => $data['code'],
                'chuli_type' => $data['chuli_type'],
                'receipt' => $data['receipt'],
                'intelligence' => json_encode($data['intelligence']),
                'subpackage' => $data['subpackage'],
                'format' => $data['format'],
                'way' => $data['way'],
                'urgent' => $data['urgent'],
                'file' => $data['file'],
                'order_Sn' => time()
            ];
            $xiangmu = [
                'province' => $data['data']['province'],
                'city' => $data['data']['city'],
                'area' => $data['data']['area'],
                'caizhi' => $data['data']['caizhi'],
                'xinghao' => $data['data']['xinghao'],
                'other' => $data['data']['other'],
                'num' => $data['data']['num'],
                'content' => $data['data']['content'],
            ];
            db('orders')->insert($order);
            $order_id = db('orders')->getLastInsID();
            if ($order_id) {
                $xiangmu['order_id'] = $order_id;
                $good_id = db('order_goods')->insert($xiangmu);
                if ($good_id) {
                    $result = [
                        'code' => '1',
                        'msg' => '发布成功',
                        'order_id' => $order['order_Sn'],
                        //插入成功匹配实验室
                        'sys' => $this->searchsya($lat, $lng)
                    ];
                    return json($result);
                }
            }
        }


    }

    function searchsys($lat = null, $lng = null)
    {    //获取所有实验室，进行距离最近匹配
        $list = db('sysrz')->where('logo <> "" and status=1')->select();
        $res = $this->range($lat, $lng, $list);
        return json($res);
    }


    function range($u_lat, $u_lon, $list)
    {
        if (!empty($u_lat) && !empty($u_lon)) {
            foreach ($list as $row) {
                $arr = array();
                try{
                    $arr = $this->addresstolatlag($row['address']);
                }catch(\Exception $err){
                    continue;
                }
                $range = GetShortDistance($arr['lng'],$arr['lat'],$u_lon,$u_lat);
//                $row['km'] = $this->nearby_distance($u_lat, $u_lon, $arr['lat'], $arr['lng']);
                $row['km'] = $range;
                $row['km'] = round($row['km'], 1);
                $res[] = $row;
            }

            if (!empty($res)) {
                foreach ($res as $user) {
                    $ages[] = $user['km'];
                }
                array_multisort($ages, SORT_ASC, $res);
                return $res;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    //计算经纬度两点之间的距离
    public function nearby_distance($lat1, $lon1, $lat2, $lon2)
    {
        $ak = '1USfemxk5GDBefhHMVY9khbp1xbjkee9';//您的百度地图ak，可以去百度开发者中心去免费申请
        $distance = array();
        $distance['distance'] = 0.00;//距离 公里
        $distance['duration'] = 0.00;//时间 分钟
        $url = 'http://api.map.baidu.com/routematrix/v2/driving?output=json&origins='.$lat1.','.$lon1.'&destinations='.$lat2.','.$lon2.'&ak='.$ak;
        $data = file_get_contents($url);
        $data = json_decode($data,true);
        if (!empty($data) && $data['status'] == 0) {
            $distance['distance'] =preg_replace('/[^\.0123456789]/s', '', $data['result'][0]['distance']['text']); //计算距离
        }
        return $distance['distance'];
    }

    function addresstolatlag($address){

        $url='http://api.map.baidu.com/geocoder/v2/?address='.$address.'&output=json&ak=1USfemxk5GDBefhHMVY9khbp1xbjkee9';
        if($result=file_get_contents($url))
        {
            $res = json_decode($result,true);
            return $res['result']['location'];
        }
    }



}



?>