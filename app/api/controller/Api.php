<?php

namespace app\api\controller;

use think\Controller;

class Api extends Controller
{

//    首页幻灯片ad

    function banner()
    {

//        $adList = cache('adList');

//        if (!$adList) {
//
//            $adList = db('ad')->where(['type_id' => 1, 'open' => 1])->order('sort asc')->limit('10')->select();
//
////            cache('adList', $adList, 3600);
//
//        }
        $adList = db('ad')->where(['type_id' => 1, 'open' => 1])->order('sort asc')->limit('10')->select();
        return msg($adList);

    }

//    获取新闻列表articles

    function GetNews($catid = null, $page = null, $limit = 10)
    {
        $page = input('page');
        if ($page == null) {
            $page = 1;
        }
        $where = array();

        $list = db('articles')->where($where)->field('id,catid,title,thumb,content,username,createtime,updatetime')->order('id desc')->page($page, $limit)->select();

        return msg($list, count($list));
    }

//    获取新闻详情articles

    function GetContent($id = null)
    {

        $content = db('articles')->where('id', $id)->column('content');
        $haha = htmlspecialchars_decode($content[0]);
        $this->assign(['html' => $haha]);
        return view('index');

    }

//    用户登陆users

    function login()
    {

        $table = db('users');

        $data = input();

        $username = $data['phone'];

        $password = $data['pwd'];

        $is_sys = 0;

        $user = $table->where("mobile", $username)->whereOr('email', $username)->find();

        $sys_count = db('sysrz')->where('uid', $user['id'])->count();

        if($sys_count)  $is_sys = 1;

        if (!$user) {

            return msg('', '', 0, '账号不存在');

        } elseif (md5($password) != $user['password']) {

            return msg('', '', 0, '密码错误');

        } elseif ($user['is_lock'] == 1) {

            return msg('', '', 0, '账号异常已被锁定');

        } else {

            $user['is_sys'] = $is_sys;

            return msg($user);


        }

    }

    // 用户注册users
    function register()
    {

        $data = input();
        // 判断手机号是否存在
        $res = db('users')->where('mobile', $data['mobile'])->find();
        if ($res) {
            return msg('', '', 0, '您已注册，请登录！');
        }
        $data['reg_time'] = time();
        $data['password'] = md5($data['password']);
        $re = db('users')->insertGetId($data);
        $user = db('users')->find($re);
        return msg($user);
    }

    /**
     * Notes: 第三方登录
     * Databases: users
     * User: Ericx
     * Date: 2018/8/6
     * Time: 10:18
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    function third_login()
    {
        $data = input();
        if(!$data['openid']){
            return msg('','',0,'获取openid失败');
        }
        // 判断手机号是否存在
        $user = db('users')->where('openid', $data['openid'])->find();
        if ($user) {
            return msg($user);
        }
        $data['username'] = input('username');
        $data['avatar'] = input('avatar');
        $data['sex'] = input('sex');
        $data['mobile'] = input('mobile');
        $data['province'] = input('province');
        $data['city'] = input('city');
        $data['district'] = input('district');
        $data['reg_time'] = time();
        $re = db('users')->insertGetId($data);
        $user = db('users')->find($re);
        return msg($user);
    }



//    获取检测项目分类jc_category

    function get_jc_category($catid = null)
    {

        if ($catid == null) {

            $catid = 0;

        }

        $p = db('jc_category')->where('pid', $catid)->select();
        $shixiao = db('jc_category')->find(6);
        if($catid != -1) array_push($p,$shixiao);//失效
        if($catid == 0 || $catid == -1) array_push($p,['id' => '-1', 'pid' => 0, 'name'=>'其他', 'img_url'=>'http://m.cele.me/upload/201701/22/201701221529542826.png']);//其他

        return msg($p);

    }

    //    获取检测项目jcxiangmu
    function get_jc_xiangmu($page = null)
    {
        $limit = 10;
        $page = input('page');
        if ($page == null) {
            $page = 1;
        }
        $catid = input('catid');
        $keyword = input('keyword');
        if(!$keyword){
            if($catid == -1){
                $p = db('jcxiangmu')
                    ->page($page, $limit)
                    ->select();
            }else{
                $p = db('jcxiangmu')->where('catid', $catid)
                    ->page($page, $limit)
                    ->select();
            }

        }else{
            $p = db('jcxiangmu')
                ->where('title|description|content|keywords', 'like', '%' . $keyword . '%')
                ->page($page, $limit)
                ->select();
        }

        foreach($p as $k => $v){

            $p[$k]['categoory'] = db('jc_category')->find($v['catid']);
        }

        return msg($p);

    }

    //  获取实验室sysrz
    function getsys()
    {

        $page = input('page');
        if ($page == null) {
            $page = 1;
        }
        $keyword = input('keyword');
        $where = array();

        $list = db('sysrz')
            ->where('status', 1)
            ->where('name|desc|jcnl|ywsm|cases', 'like', '%' . $keyword . '%')
            ->order('id desc')->paginate(10, false, ['page' => $page]);

        return msg($list, count($list));
    }

    //发布需求orders
    function fbxq()
    {

        $data = input();

        db('xqlist')->where('uid',$data['uid'])->delete();

        $order_id = isset($data['order_id'])?$data['order_id']:0;

        $order_info = db('orders')->find($order_id);

        $order = [

            'title' => $data['title'],

            'uid' => $data['uid'],

            'endtime' => strtotime($data['endtime']),

            'time' => time(),

            'company' => $data['company'],

            'address' => $data['address'],

            'r_name' => $data['r_name'],

            'r_address' => $data['r_address'],

            'r_phone' => $data['r_phone'],

            'code' => $data['code'],

            'chuli_type' => $data['chuli_type'],

            'receipt' => $data['receipt'],

            'intelligence' => json_encode($data['intelligence']),

            'subpackage' => $data['subpackage'],

            'format' => $data['format'],

            'way' => $data['way'],

            'urgent' => $data['urgent'],

            'file' => $data['file'],

            'tatchsn' => $data['tatchsn'],

            'jc_cate' => $data['jc_cate']

        ];

        $xiangmu = [

            'province' => $data['province'],

            'city' => $data['city'],

            'area' => $data['area'],

            'caizhi' => $data['caizhi'],

            'xinghao' => $data['xinghao'],

            'other' => $data['other'],

            'num' => $data['num'],

            'content' => $data['content'],

        ];


        if($order_id && $order_info){

            if($order_info['status'] > 0){

                $result = [

                    'status' => '0',

                    'msg' => '修改失败，您的订单已被受理'

                ];

                return json($result);
            }


            $order_update_msg = db('orders')->where('id',$order_info['id'])->update($order);

            if($order_update_msg){

                $result = [

                    'status' => '1',

                    'msg' => '修改成功',

                    'order_id' => $order_info['order_Sn']

                ];

            }else{

                $result = [

                    'status' => '0',

                    'msg' => '修改失败'

                ];

            }
            return json($result);


        }else{

            $order['order_Sn'] = $this->makeOrderSn();


            db('orders')->insert($order);

            $order_id = db('orders')->getLastInsID();

            if ($order_id) {

                $xiangmu['order_id'] = $order_id;

                $good_id = db('order_goods')->insert($xiangmu);

                if ($good_id) {

                    $result = [

                        'status' => '1',

                        'msg' => '发布成功',

                        'order_id' => $order['order_Sn']

                    ];

                    return json($result);

                }

            }
        }




    }

    //最新需求列表orders
    function newest_xuqiu()
    {

        $limit = 10;
        $page = input('page');
        $keyword = input('keyword');
        $catid = input('catid/d');
        $user_id = input('user_id/d');
        if ($page == null) {
            $page = 1;
        }
        if ($catid == null) {
            $list = db('orders')
                ->where('title', 'like', '%' . $keyword . '%')
                ->where('status','in',[1,2,5,6])
                ->order('status','desc')->order('id desc')->page($page, $limit)
                ->select();
        } else {
            $re = db('order_goods')->where('city', $catid)->select();
            foreach ($re as $item) {
                $order_sn[] = $item['order_id'];
            }
            $list = db('orders')->where('id', ['IN', $order_sn])
                ->where('title', 'like', '%' . $keyword . '%')
                ->where('status','in',[1,2,5,6])
                ->order('status','desc')->order('id desc')->page($page, $limit)
                ->select();
        }

        if($user_id > 0){

            foreach($list as $k=>$v){

                $is_my_receive = 0;

                $res = db('user_order')->where('order_id',(int)$v['id'])->where('user_id',$user_id)->count();

                if($res > 0) $is_my_receive = 1;

                $list[$k]['is_my_get'] = $is_my_receive;

            }

        }

        return msg($list, count($list));

    }

    //我的需求列表orders
    function my_xuqiu()
    {

        $uid = input('uid');
        $limit = 10;
        $page = input('page');
        if ($page == null) {
            $page = 1;
        }

//        $list = db('orders')->where('uid', $uid)->where('status','neq',6)
        $list = db('orders')->where('uid', $uid)
            ->order('id desc')->page($page, $limit)
            ->select();

        foreach ($list as &$item){
//            $res = db('user_order')->where('order_id',$item['id'])->find();
//            if ($res)   $item['status'] = 7;//已接单
            $item['jindu'] = db('order_jindu')->where('order_id', $item['id'])->order('id', 'desc')->select();
        }
        return msg($list, count($list));

    }

    //我的订单列表user_order orders
    function my_order()
    {

        $uid = input('uid');
        $limit = 10;
        $page = input('page');
        if ($page == null) {
            $page = 1;
        }
        //查询关联表
        $order_ids = db('user_order')->where('user_id', $uid)->column('order_id');

        $list = db('orders')->where('id', 'in', $order_ids)
            ->order('id desc')->page($page, $limit)
            ->select();

        if (count($list) > 0) {
            return msg($list, count($list));
        } else {
            return msg('', '', 0, '暂无信息');
        }

    }

    // 获取用户信息users
    function login_user()
    {

        $is_sys = 0;

        $uid = input('uid/d');

        $table = db('users');

        $sys = db('sysrz')->where('uid',$uid)->find();

        if($sys) $is_sys = 1;

        $user = $table->find($uid);

        $user['is_sys'] = $is_sys;

        return msg($user);
    }

    function sys_isrz(){
        $uid = input('uid');
        $is_rz = db('sysrz')->where('uid',$uid)->find();

        if($is_rz){

            if($is_rz['status'] == 1){
                $result = [
                    'data'=>['status'=>1],
                    'status' =>1,

                    'msg' => '您已通过审核'

                ];
            }else{
                $result = [
                    'data'=>['status'=>2],
                    'status' => 1,

                    'msg' => '您未通过审核'

                ];
            }


        }else{

            $result = [
                'data'=>['status'=>0],
                'status' => 1,

                'msg' => '您还没有入驻实验室'

            ];
        }

        return json($result);
    }

    function edit_sys()
    {

        $data = input();
        $insert_data = [
            'uid' => $data['uid'],
            'catid' => $data['catid'],
            'name' => $data['name'],
            'tel' => $data['tel'],
            'email' => $data['email'],
            'province' => $data['province'],
            'city' => $data['city'],
            'area' => $data['area'],
            'address' => $data['address'],
            'username' => $data['username'],
            'mobile' => $data['mobile'],
            'jcnl' => $data['jcnl'],
            'desc' => $data['desc'],
            'ywsm' => $data['ywsm'],
            'cases' => $data['cases'],
            'yyzz' => $data['yyzz'],
            'zzry' => $data['zzry'],
            'position' => $data['position'],
            'logo' => $data['logo'],
            'location' => $data['location'],
        ];

        $insert_msg = db('sysrz')->where('id',$data['id'])->update($insert_data);

        if ($insert_msg) {

            $result = [
                'status' => '1',

                'msg' => '入驻成功'
            ];


        } else {

            $result = [

                'status' => '0',

                'msg' => '入驻失败'

            ];
        }
        return json($result);

    }

    function get_usersys(){
        $uid = input('uid');

        $is_rz = db('sysrz')->where('uid',$uid)->find();

        if($is_rz){

            $result = [
                'data'=>$is_rz,

                'status' => '1',

                'msg' => '获取成功'

            ];
            return json($result);
        }
    }

    // 实验室入驻sysrz
    function sys_rz()
    {

        $data = input();

        $is_rz = db('sysrz')->where('uid',$data['uid'])->find();

        if($is_rz){

            $result = [

                'status' => '1',

                'msg' => '入驻实验室失败，您已成功入驻其他实验室'

            ];
            return json($result);
        }

        $insert_data = [
            'uid' => $data['uid'],
            'catid' => $data['catid'],
            'name' => $data['name'],
            'tel' => $data['tel'],
            'email' => $data['email'],
            'province' => $data['province'],
            'city' => $data['city'],
            'area' => $data['area'],
            'address' => $data['address'],
            'username' => $data['username'],
            'mobile' => $data['mobile'],
            'jcnl' => $data['jcnl'],
            'desc' => $data['desc'],
            'ywsm' => $data['ywsm'],
            'cases' => $data['cases'],
            'yyzz' => $data['yyzz'],
            'zzry' => $data['zzry'],
            'position' => $data['position'],
            'logo' => $data['logo'],
            'location' => $data['location'],
        ];

        $insert_msg = db('sysrz')->insertGetId($insert_data);

        if ($insert_msg) {

            $result = [

                'status' => '1',

                'msg' => '入驻成功'

            ];


        } else {

            $result = [

                'status' => '0',

                'msg' => '入驻失败'

            ];
        }
        return json($result);

    }

    //实验室分类jc_category
    function sys_cate()
    {
        $list = db('jc_category')->where('pid', 0)->select();
        return msg($list);
    }

    // 我要留言feedback
    function user_message()
    {
        $data = input();
//        foreach (array_values($data) as $item){
//            if(!isNull($item)){
//                return msg('','',0,'数据不能为空,请检查输入的信息');
//            };
//        }
        $insert_data = [
            'user_id' => $data['uid'],//
            'title' => $data['title'],// 标题
            'desc' => $data['desc'], //
            'reply' => $data['reply'], //
            'reply_user' => $data['reply_user'], //
            'imgs' => $data['reply_phone'], //手机号
            'add_time' => time()
        ];
        $res = db('feedback')->insertGetId($insert_data);
        if ($res > 0) {
            return msg('', '');
        } else {
            return msg('', '', 0, '提交失败,请重新提交');
        }
    }

    // 检测设备zizhinengli
    function check_shebei()
    {
        $page = input('page');
        $page == null ? $page = 1 : $page;
        $res = db('zizhinengli')->where('catid', 30)->order('id desc')->page($page, 10)->select();
        if (count($res)) {
            return msg($res, count($res));
        } else {
            return msg('', '', 0, '请求失败');
        }
    }

    //设备详情zizhinengli
    function shebei_detail()
    {
        $id = input('id');
        $content = db('zizhinengli')->where('id', $id)->column('content');
        $haha = htmlspecialchars_decode($content[0]);
        $this->assign(['html' => $haha]);
        return view('index');
    }

    /**
     * Notes: 根据批次号获取 线下订单详情 offline_orders
     * User: Ericx
     * Date: 2018/7/23
     * Time: 17:09
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     **/
    function offline_order()
    {

        $tatchsn = input('tatchsn');
        $data = [];
        $info = db('offline_orders')->where('tatchsn', $tatchsn)->whereOr('reportsn',$tatchsn)->find();
        $info1 = db('orders')->where('order_Sn',$tatchsn)->find();
        if ($info==null && $info1 == null){
            return msg('', '', 0, '查无此订单');
        }elseif ($info !=null && $info1 == null){
            $jindu_list = db('order_jindu')->where('offline_order_id', $info['id'])->order('id', 'desc')->select();
        }elseif ($info ==null && $info1 != null){
            $jindu_list = db('order_jindu')->where('order_id', $info1['id'])->order('id', 'desc')->select();
        }else{
            return msg('', '', 0, '订单信息错误');
        }
        if(!$jindu_list){
            return msg('', '', 0, '暂无进度信息');
        }
        if($info) $data = $info;
        if($info1) $data = $info1;
        $data['jindu'] = $jindu_list;
        return msg($data);
    }

    /**
     * Notes: 线下订单进度查询 order_jindu
     * params order_id
     * User: Ericx
     * Date: 2018/7/23
     * Time: 17:16
     */
    function order_jindu()
    {

        $order_id = input('order_id/d');
        $jindu_list = db('order_jindu')->where('offline_order_id', $order_id)->order('id', 'desc')->select();
        if (!$jindu_list) return msg('', '', 0, '暂无进度');
        return msg($jindu_list);
    }

    //实验室分类jc_category
    function get_cates()
    {
        $list = db('jc_category')->order('id', 'desc')->select();
        return msg($list);
    }

    // 发布检测项目sysrz xiangmu
    function publish_project()
    {

        // 获取实验室ID
        $sid = db('sysrz')->where('uid', input('uid/d'))->value('id');
        if (!$sid) {
            return msg('', '', 0, '您还没有实验室信息，请先在首页入住实验室');
        }
        $data = [
            'sid' => $sid,
            'jid' => input('jid'),
            'jcyj' => input('jcyj'),
            'desc' => input('desc'),
        ];
        $insert_msg = db('xiangmu')->insertGetId($data);
        if ($insert_msg) {
            return msg('', '');
        }
        return msg('', '', 0, '发布失败');
    }

    /**
     * Notes: 检测项目列表sysrz xiangmu
     * User: Ericx
     * Date: 2018/7/24
     * Time: 10:38
     */
    function project_list()
    {

        $page = input('page');
        $page == null ? $page = 1 : $page;
        // 获取实验室ID
        $sid = db('sysrz')->where('uid', input('uid/d'))->value('id');
        if (!$sid) {
            return msg('', '', 0, '获取实验室信息id失败');
        }

        $list = db('xiangmu')->where('sid', $sid)->order('sort')->page($page, 10)->select();
        return msg($list, count($list));
    }

    /**
     * Notes: 添加设备 shebei
     * User: Ericx
     * Date: 2018/7/24
     * Time: 10:42
     */
    function add_equipment()
    {

        $uid = input('uid/d');
        if (!$uid) return msg('', '', '获取用户信息失败');
        $data = [
            'name' => input('name'),
            'canshu' => input('canshu'),
            'uid' => input('uid'),
            'pic' => input('pic')
        ];
        $insert_msg = db('shebei')->insertGetId($data);
        if ($insert_msg) return msg('', '');
        return msg('', '', 0, '添加设备失败');
    }

    /**
     * Notes: 获取我的设备列表 shebei
     * User: Ericx
     * Date: 2018/7/24
     * Time: 10:49
     * @return \think\response\Json
     */
    function equipment_list()
    {

        $limit = 10;
        $page = input('page');
        $uid = input('uid/d');
        if ($page==null)$page = 1;
        if (!$uid) return msg('', '', '获取用户信息失败');
        $list = db('shebei')->where('uid', $uid)->order('id', 'desc')->page($page,$limit)->select();
        return msg($list);
    }

    /**
     * Notes: 我的实验室 sysrz
     * User: Ericx
     * Date: 2018/7/24
     * Time: 10:53
     */
    function my_sys()
    {

        $uid = input('uid/d');
        if (!$uid) return msg('', '', '获取用户信息失败');
        $info = db('sysrz')->where('uid', $uid)->find();
        return msg($info);
    }

    // 关于我们page
    function about_us()
    {

        $info = db('page')->find(2);
        return msg($info);
    }

    // 联系我们page
    function contract_us()
    {
        $info = db('page')->find(39);
        return msg($info);
    }

    // 检测指南page
    function jc_zhinan()
    {
        $info = db('page')->find(24);
        $this->assign(['html'=>$info['content']]);
        return view('index');
//        return msg($info);
    }

    //修个人信息users
    function edit_info()
    {

        $uid = input('uid/d');
        $user_info = db('users')->find($uid);
        if (!$user_info) return msg('', '', '获取用户信息失败');

        $data = [
            'avatar' => input('avatar'),
            'mobile' => input('mobile'),
            'email' => input('email'),
        ];
        foreach ($data as $k => $item) {
            if (!$item) unset($data[$k]);
        }
        $update_msg = db('users')->where('id', $uid)->update($data);
        if ($update_msg) return msg('', '');
        return msg('', '', 0, '修改失败');
    }

    // 上传图片处理
    public function upload()
    {

        $fileKey = array_keys(request()->file());
        // 获取表单上传文件
        $file = request()->file($fileKey['0']);
        // 移动到框架应用根目录/public/uploads/ 目录下
        $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
        if ($info) {
            $result['code'] = 0;
            $result['msg'] = '图片上传成功!';
            $path = str_replace('\\', '/', $info->getSaveName());
            $result["src"] = '/uploads/' . $path;
            return msg($result);
        } else {
            // 上传失败获取错误信息
            $result['code'] = 1;
            $result['msg'] = '图片上传失败!';
            return msg($result);
        }
    }

    /**
     * Notes: 订单支付成功接口orders
     * User: Ericx
     * Date: 2018/7/24
     * Time: 15:08
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    function order_ispay()
    {

        $order_sn = input('order_sn');
        $order_info = db('orders')->where('order_Sn', $order_sn)->find();
        if (!$order_info) {
            $result['code'] = 1;
            $result['msg'] = '获取订单信息失败!';
            return msg($result);
        }
        if($order_info['status'] == 5){
            $result['code'] = 1;
            $result['msg'] = '订单已支付，请勿重复支付!';
            return msg($result);
        }
        $update_msg = db('orders')->where('order_Sn', $order_sn)->setField('status', 5);
        if ($update_msg) {
            $result['code'] = 0;
            $result['msg'] = '订单已支付!';
        }else{
            $result['code'] = 1;
            $result['msg'] = '订单支付失败!';
        }
        return msg($result);

    }

    /**
     * Notes: 接单接口 user_order orders
     * User: Ericx
     * Date: 2018/7/28
     * Time: 14:40
     * @return mixed
     */
    function getorder()
    {
        $user_id = input('uid');

        $order_id = input('order_id');

        $order = db('orders')->find($order_id);

        $yz = db('user_order')->where('order_id' , $order_id)->find();

        if ($yz) {

            return msg('', '', 0, '该订单已被接单，不能重复接单');
        }

        // 判断是否入驻实验室
        $is_sys = db('sysrz')->where('uid',$user_id)->find();
        if(!$is_sys) return msg('', '', 0, '个人用户不能接单，只有实验室可以接单');

        if($user_id == $order['uid'])

            return msg('', '', 0, '不能接自己的单');

        $res = db('user_order')->insertGetId(['user_id' => $user_id, 'order_id' => $order_id]);

        db('orders')->where('id', $order_id)->setField('status', 1);

        if ($res > 0) {

            return msg('', '');
        } else {

            return msg('', '', 0, '接单失败');
        }
    }

    /**
     * 获取APP通知消息page
     * @return mixed
     */
    function getAppMessage()
    {
        $uid = input('uid'); //具体到每个人的通知消息
        $uid = input('uid');
        db('message')->where(['uid'=>$uid])->setField('is_read',1);
        $page = input('page');
        $limit = 10;
        if ($page == null) {
            $page = 1;
        }
        $msg = db('message')->where('uid',$uid)->whereOr('uid',0)->order('addtime desc')->page($page, $limit)->select();
        return msg($msg);
    }

    function is_readMessage(){
        $uid = input('uid');
        $msg = db('message')->where(['uid'=>$uid,'is_read'=>0])->select();
        if (count($msg) > 0){
            return msg('',0,1);
        }else{
            return msg('',0,0);
        }
    }

    /**
     * Notes: 退单接口 user_order orders
     * User: Ericx
     * Date: 2018/7/28
     * Time: 14:15
     * @return mixed
     */
    function reback_order()
    {
        $user_id = input('uid');

        $order_id = input('order_id');

        $order = db('orders')->find($order_id);

        db('user_order')->where(['order_id'=>$order_id,'user_id'=>$user_id])->delete();

        if($order['status'] == 4){

            return msg('', '', 0, '该订单已取消，不能重复退单');
        }

        if($order['apply_refund'] == 1){

            return msg('', '', 0, '退单申请后台审核中。。。');
        }

        if($order['status']>0){

            $res = db('orders')->where('id', $order_id)->setField('apply_refund', 1);//退单申请中

        }else{
            
            $res = db('orders')->where('id', $order_id)->setField('status', 4);//用户取消订单

        }

        if ($res > 0) {

            return msg('', '');

        } else {

            return msg('', '', 0, '退单失败');

        }

    }

    // 订单详情orders order_goods
    function order_detail(){

        $order = db('orders')->find(input('order_id/d'));

        if(!$order) return msg('', '', 0, '查询订单失败');

        $order_goods = db('order_goods')->where('order_id', $order['id'])->find();

        $order['order_goods'] = $order_goods;

        return msg($order);

    }

    function smscode(){

        $mobile = input('mobile');

        $code =  rand(1000,9999);

        $statusStr = array(
            "0" => "短信发送成功",
            "-1" => "参数不全",
            "-2" => "服务器空间不支持,请确认支持curl或者fsocket，联系您的空间商解决或者更换空间！",
            "30" => "密码错误",
            "40" => "账号不存在",
            "41" => "余额不足",
            "42" => "帐户已过期",
            "43" => "IP地址限制",
            "50" => "内容含有敏感词"
        );

        $smsapi = "http://api.smsbao.com/";
        $user = "htzj"; //短信平台帐号
        $pass = md5("htzj123456"); //短信平台密码
        $content="【航天智检】您的验证码为{$code}";//要发送的短信内容
        $phone = $mobile;//要发送的手机号码
        $sendurl = $smsapi."sms?u=".$user."&p=".$pass."&m=".$phone."&c=".urlencode($content);
        $result =file_get_contents($sendurl) ;
        $sms_data = [
            'mobile' => $phone,
            'code' => $code,
            'result' => $statusStr[$result],
            'created_at' => date('Y-m-d H:i:s')
        ];
        $insert_msg = db('sms_log')->insertGetId($sms_data);

        if($insert_msg){

            return msg('', '', 1, $statusStr[$result]);
        }else{

            return msg('', '', 0, $statusStr[$result]);
        }

    }

    //重置密码
    function change_pwd(){

        $code = input('code');
        $mobile = input('mobile');
        $password = input('password');

        $smscode = db('sms_log')->where('mobile', $mobile)->value('code');

        if($code != $smscode){

            return msg('','',0,'验证码错误');
        }

        db('sms_log')->where('mobile', $mobile)->delete();

        $update_msg = db('users')->where('mobile', $mobile)->setField('password', md5($password));

        if($update_msg){

            return msg('','',1,'密码重置成功');
        }else{

            return msg('','',0,'密码重置失败');
        }

    }

    /**
     * Notes: 添加主题
     * Databases: topic
     * User: Ericx
     * Date: 2018/8/6
     * Time: 9:49
     * @return \think\response\Json
     */
    public function add_topic()
    {
        $uid = input('uid');
        $content = input('content');

        if(!$uid)   return msg('','',0,'获取用户id失败');
        if(!$content)   return msg('','',0,'获取主题内容失败');

        $data = [
            'content' => $content,
            'add_time' => time(),
            'img' => input('img'),
            'uid' => $uid,
        ];

        $insert_msg = db('topic')->insertGetId($data);

        if($insert_msg){

            return msg('','',1,'帖子已移交，等待审核');
        }else{

            return msg('','',0,'帖子发布失败');
        }

    }

    /**
     * Notes: 主题回复
     * Databases: reply
     * User: Ericx
     * Date: 2018/8/6
     * Time: 9:49
     * @return \think\response\Json
     */
    public function add_reply()
    {

        $uid = input('uid');
        $topic_id = input('topic_id');
        $reply_content = input('reply_content');
        $topic = db('topic')->find($topic_id);
        $user_name = db('users')->where('id',$uid)->value('username');

        if(!$uid)   return msg('','',0,'获取用户id失败');
        if(!$topic_id)   return msg('','',0,'获取帖子id失败');
        if(!$topic)   return msg('','',0,'获取帖子失败');
        if(!$reply_content)   return msg('','',0,'获取回复内容失败');

        $data = [
            'topic_id' => $topic_id,
            'reply_content' => $reply_content,
            'uid' => $uid,
            'add_time' => time(),
        ];

        $insert_msg = db('reply')->insertGetId($data);

        if($insert_msg){
            //添加提示信息
            db('message')->insertGetId([
                'title' => '您的帖子"'.$topic['content'].'"得到了回复',
                'addtime' => time(),
                'content' => '"'.$user_name.'"评论了:"'.$reply_content.'"',
                'name' => $user_name,
                'uid' => isset($topic['uid'])?$topic['uid']:0
            ]);
            return msg('','',1,'回复成功');
        }else{

            return msg('','',0,'回复失败');
        }

    }

    /**
     * Notes: 测试圈消息列表
     * Databases: topic reply
     * User: Ericx
     * Date: 2018/8/6
     * Time: 9:53
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function topic_list()
    {
        $limit = 10;
        $page = input('page');
        if ($page==null)$page = 1;
        $list = db('topic')->where('status',1)->order('id', 'desc')->page($page,$limit)->select();
        foreach ($list as $k=>$v){
            $topic_user = db('users')->find($v['uid']);//主题用户信息
            $list[$k]['username'] = mb_substr($topic_user['username'],0,1,'utf-8').'**';
            $list[$k]['avatar'] = $topic_user['avatar'];
            $reply_list = db('reply')->where('topic_id', $v['id'])->select();
            if($reply_list){

                foreach ($reply_list as $m=>$n){
                    $reply_user = db('users')->find($n['uid']);//回复用户信息
                    $reply_list[$m]['username'] = mb_substr($reply_user['username'],0,1,'utf-8').'**';
                    $reply_list[$m]['avatar'] = $reply_user['avatar'];
                }
                $list[$k]['reply'] = $reply_list;
            }else{

                $list[$k]['reply'] = [];
            }

        }
        return msg($list);
    }

    public function my_topic()
    {
        $limit = 10;
        $page = input('page');
        $uid = input('uid');
        if ($page==null)$page = 1;
        $list = db('topic')->where(['status'=>1,'uid'=>$uid])->order('id', 'desc')->page($page,$limit)->select();
        foreach ($list as $k=>$v){
            $topic_user = db('users')->find($v['uid']);//主题用户信息
            $list[$k]['username'] = mb_substr($topic_user['username'],0,1,'utf-8').'**';
            $list[$k]['avatar'] = $topic_user['avatar'];
            $reply_list = db('reply')->where('topic_id', $v['id'])->select();
            if($reply_list){

                foreach ($reply_list as $m=>$n){
                    $reply_user = db('users')->find($n['uid']);//回复用户信息
                    $reply_list[$m]['username'] = mb_substr($reply_user['username'],0,1,'utf-8').'**';
                    $reply_list[$m]['avatar'] = $reply_user['avatar'];
                }
                $list[$k]['reply'] = $reply_list;
            }else{

                $list[$k]['reply'] = [];
            }

        }
        return msg($list);
    }

    //app版本列表
    public function app_list()
    {
        $list = db('app_version')->order('id','desc')->find();
        return msg($list);
    }

    //资质证书,不分页
    public function zznl()
    {

        $lists = db('zizhinengli')->where('catid',28)->order('listorder','desc')->order('id','desc')->select();
        return msg($lists);
    }

    // 业务范围，分页
    public function ywfw()
    {

        $page = input('page');

        $limit = 10;

        if ($page == null) {

            $page = 1;

        }

        $msg = db('zizhinengli')->where('catid',29)->order('listorder','desc')->order('id','desc')->page($page, $limit)->select();

        return msg($msg);
    }

    //添加订单进度
    public function add_jindu()
    {

        $data = input();

        $order_info = db('orders')->find($data['order_id']);

        if(!$order_info){

            return msg('','',0,'订单信息有误，查无此订单');
        }
        if(!$data['desc']){

            return msg('','',0,'进度描述不能为空');
        }

        $insertData = [
            'order_id' => $data['order_id'],
            'desc' => $data['desc'],
            'file' => $data['file'],
            'time' => date('Y-m-d H:i:s')
        ];

        $insert_msg = db('order_jindu')->insertGetId($insertData);
        if($insert_msg){
            return msg('','',1,'添加进度成功');
        }else{
            return msg('','',0,'添加进度失败');
        }
    }

    public function makeOrderSn($sn = null)
    {
        /* 选择一个随机的方案 */
        mt_srand((double)microtime() * 1000000);
        if ($sn)
            return substr($sn, 0, 14) . str_pad(mt_rand(1, 99999), 6, '0', STR_PAD_LEFT);
        return date('YmdHis') . str_pad(mt_rand(1, 99999), 6, '0', STR_PAD_LEFT);
    }

    public function add_xqname()
    {
        $data = input();
        $res = db('xqlist')->where(['uid'=>$data['uid'],'row'=>$data['row']])->find();
        if ($res){
            db('xqlist')->where(['uid'=>$data['uid'],'row'=>$data['row']])->update($data);
        }else{
            db(xqlist)->insert($data);
        }
        return json(['code'=>1]);
    }

    public function get_xqname(){
        $uid = input('uid');
        $res = db('xqlist')->field('name,num')->where('uid',$uid)->select();
        return json($res);
    }

    function delete_xq(){
        $uid = input('uid');
        $row = input('row');
        $res = db('xqlist')->where(['row'=>$row,'uid'=>$uid])->delete();
        return json(['code'=>1]);
    }

}
