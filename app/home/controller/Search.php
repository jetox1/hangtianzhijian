<?php

namespace app\home\controller;

use think\Db;



class Search extends Common{

    function search($order_id){

        $order_sn = htmlspecialchars($order_id);

        $info = db('orders')->where('order_Sn',$order_sn)->find();
        $offlineinfo = db('offline_orders')->where('tatchsn|reportsn',$order_sn)->find();
        if ($info == null){
            if($offlineinfo == null){
                return $this->error('该订单编号不存在...');
            }else{
                $jindu = get_offline_jindu($offlineinfo['id']);
                $this->assign('jindu',$jindu);
            }
        }else{

            $jindu = get_jindu($info['id']);
            $this->assign('jindu',$jindu);

        }

        return view();

    }

}