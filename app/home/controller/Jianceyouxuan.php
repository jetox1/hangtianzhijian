<?php
namespace app\home\controller;
class Jianceyouxuan extends Common{
    public function _initialize(){
        parent::_initialize();
    }
    public function index(){
        return view('page_youxuan');
    }
    public function search(){
            $keywords = input('post.keywords');
            if ($keywords!=null||$keywords!=''){
                $map['title']=array('like','%%'.$keywords.'%%');
            }
            $list = db('jcxiangmu')->where($map)->select();
            $this->assign('lists',$list);
            return view();
    }
    public function ditu(){
        return view();
    }
    public function dingxiang($catid=null){
        $one = db('jc_category')->where('pid','0')->select();
        if ($catid!=null){
            $lists = db('jcxiangmu')->where('catid',$catid)->select();
            $topcategory = db('jc_category')->where('id',$catid)->find();
            $this->assign('topid',$topcategory['pid']);
            $this->assign('catid',$catid);
            $this->assign('lists',$lists);
        }else{
            $lists = db('jcxiangmu')->where('catid',2)->select();
            $this->assign('lists',$lists);
        }
        $this->assign('first',$one);
        return view();
    }
    public function show($id=null){
        $info =db('jcxiangmu')->where('id',$id)->find();
        $info['catename'] = $this->get_catname($info['catid']);
        $info['parent'] = $this->get_parent($info['catid']).'-'.$info['title'];
        $this->assign('info',$info);
        return view();
    }
    function get_catname($catid=null){
        $catname = db('jc_category')->where('id',$catid)->column('name');
        return $catname[0];
    }
    function get_parent($catid=null){
        $cat = db('jc_category')->where('id',$catid)->find();
        $one     = db('jc_category')->where('id',$cat['pid'])->find();
        return $one['name'].'-'.$cat['name'];
    }
    public function ajaxcategory($id = null){
        $id = input('id');
        $one = db('jc_category')->where('pid',$id)->select();
        return json($one);
    }
    function ajaxxiangmu(){
        $id = input('id');
        $one = db('jcxiangmu')->where('catid',$id)->select();
        return json($one);
    }
    public function ajaxsys(){
        $list  = db('sysrz')->field('address,location')->select();
        $ccc= [];
        foreach ($list as $item){
            $location  = explode(',',$item['location']);
            $item['data']= [floatval($location[0]),floatval($location[1]),$item['address']];
            $aaa[] = $item['data'];
        }
        return json($aaa);
    }
    public function get_location($address){
        $return = file_get_contents('http://api.map.baidu.com/geocoder/v2/?address='.$address.'&output=json&ak=yXTsBdAa2G5TswAB42SNGzgZ');
        $result = json_decode($return,true);
        $location = $result['result']['location'];
        return $location;
    }
}