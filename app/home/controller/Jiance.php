<?php
namespace app\home\controller;
use think\Db;

class Jiance extends Common{
    function show($id){
        $data = db('orders')->where('id',$id)->find();
        $data['chuli_type']=get_chuli_type($data['chuli_type']);
        $data['receipt']=get_receipt($data['receipt']);
        $data['intelligence']=json_decode($data['intelligence']);
        $data['subpackage']=get_subpackage($data['subpackage']);
        $data['format']=get_format($data['format']);
        $data['way']=get_way($data['way']);
        $data['urgent']=get_urgent($data['urgent']);

        $join = [
            ['clt_jc_category w','goods.province=w.id'],
            ['clt_jc_category a','goods.city=a.id'],
            ['clt_jc_category b','goods.area=b.id'],
        ];
        $goods = Db::table('clt_order_goods')->alias('goods')
            ->join($join)
            ->where('order_id',$id)
            ->field('goods.*,w.name as province,a.name as city,b.name as area')
            ->select();
//        $goods = db('order_goods')->where('order_id',$id)->select();
        $this->assign('goods',$goods);
        $this->assign('info',$data);
        return view();
    }
}