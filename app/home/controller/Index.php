<?php
namespace app\home\controller;
class Index extends Common{
    public function _initialize(){
        parent::_initialize();
    }
    public function index(){
        if (isMobile()){
            //$this->display('app.html');
            //$this->fetch('app.html');
        return $this->fetch('index/app');
        }
        $xuqiu_count = db('orders')->count();
        $category = db('jc_category')->select();
        $list = get_attr($category,0);
        $sys = db('sysrz')->where('status','1')->select();
        $sys_data['count'] = count($sys);
        $sys_data['new'] = $sys[0]['name'];
//        $topcategory = db('jc_category')->where('pid',0)->order('sort')->limit(0,3)->select();
        $topcategory = db('jc_category')->where('pid',0)->order('sort')->select();
        foreach ($topcategory as $item){
            $item['child'] = db('jc_category')->where('pid',$item['id'])->order('sort')->limit(0,6)->select();
            $syslist=db('sysrz')->where(['catid'=>$item['id'],'position'=>1,'status'=>1])->order('id')->select();
            foreach ($syslist as $m=>$n){
                $user = db('users')->where('id',$n['uid'])->find();
                if(!$user) unset($syslist[$m]);
            }
            $item['syslist'] = $syslist;
            $topcategorys[] = $item;

        }
        $banner = db('ad')->where('type_id',10)->find();
        $this->assign('banner',$banner);
        $this->assign('categorys',$topcategorys);
        $this->assign('syss_data',$sys_data);
        $this->assign('cate_list',$list);
        $this->assign('xuqiu_count',$xuqiu_count);


        return $this->fetch();
    }

    function scsys($id=null){
        $data=array(
                'code' => $id,
                'msg'  => '验证码错误',
            );
        return json($data);
    }
    function getposxiangmu($id=null){
        $map['catid'] =$id;
        $map['posid'] =array('gt',0);
        $list = db('jcxiangmu')->where($map)->limit(0,4)->select();
        if ($list != null){
            $data['code'] = 1;
            $data['res'] = $list;
        }else{
            $data['code'] = 0;
        }

        return json($data);
    }

    function show(){
        $type = input('type');
        $res = db('article')->where('catid',$type)->find();
        $this->assign(['info'=>$res]);
        return view('article_show');
    }

    function show_page(){
        $type = input('type');
        $res = db('page')->where('id',$type)->find();
        $this->assign(['info'=>$res]);
        return view('article_show');
    }

}