<?php
namespace app\home\controller;
use think\Db;

class Sysrz extends Common{
    public function lists(){
        $data = input('post.');
        if ($data['keywords']!=''||$data['keywords']!=null){
            $map['name'] = array('like','%'.$data['keywords'].'%');
        }
        $map['status'] = 1;
        $result = db('sysrz')->where($map)->paginate(10);
        $page = $result->render();
        $this->assign('page', $page);
        $this->assign('res', $result);
        return view();
    }
    public function show($id){
        $id = intval($id);
        $info = db('sysrz')->where('id',$id)->find();
        $join = [
            ['clt_jc_category w','x.jid = w.id'],
        ];
        $result = Db::table('clt_xiangmu')->alias('x')
            ->join($join)
            ->where('sid',$id)
            ->field("x.*,w.name as jid")
            ->select();
        $uid  = db('sysrz')->where('id',$id)->field('uid')->find();
        $pic_list = db('shebei')->where('uid',$uid['uid'])->select();

        $this->assign('pic_list',$pic_list);
        $this->assign('info',$info);
        $this->assign('xiangmu_list',$result);
        return view();
    }
    function scsys($id=null){
            $data=array(
                'code' => 0,
                'msg'  => '加入收藏成功',
            );
        return json($data);
    }
}