<?php
namespace app\home\controller;
use think\Db;

class Order extends Common{
    function get_order($id=null){
        $data['order_id'] = intval($id);
        $data['user_id'] = session('user.id');
        $id = db('user_order')->insert($data);
        if ($id){
            db('orders')->update(['id'=>$data['order_id'],'status'=>1]);
            $data=array(
                'code' => 0,
                'msg'  => $id,
            );
        }else{
            $data=array(
                'code' => 0,
                'msg'  => '获取订单失败',
            );
        }

        return json($data);
    }
}