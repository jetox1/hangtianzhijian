var key_base = '2714927149271492';
var iv_base = '2714927149271492';
var key_hash = CryptoJS.MD5(key_base).toString();
var iv_hash = CryptoJS.MD5(iv_base).toString();
var key = CryptoJS.enc.Utf8.parse(key_hash.substr(0, 16));
var iv = CryptoJS.enc.Utf8.parse(iv_hash.substr(0, 16));
function _decrypt(string) {
    var data = CryptoJS.AES.decrypt(string, key, {
        iv: iv,
        padding: CryptoJS.pad.Pkcs7
    }).toString(CryptoJS.enc.Utf8);
    return data;
}
function _encrypt(string) {
    var data = CryptoJS.AES.encrypt(string, key, {
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    }).toString();
    return data;
}